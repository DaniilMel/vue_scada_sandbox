import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import ApiService from "./common/api.service";
import { initURL } from "./common/config";

import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/ru-RU'
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI,{ locale });

Vue.config.productionTip = false;

async function init() {
  await initURL().then(function (r) {
    ApiService.init(r);
    new Vue({
      router,
      store,
      render: h => h(App),
    }).$mount('#app')
  })
};

init();
