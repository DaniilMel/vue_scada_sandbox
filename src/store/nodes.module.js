import Vue from "vue";
import { AorService } from "../common/api.service";
import { ObserversObj } from "../common/observers/observers_interface";

const state = {
    nodesData: [
        {
            'groupobserver': "Math",
            'observers': [],
            'lasttime': 0,
        }
    ],
    nodes: []
}

const getters = {
    nodesData(state) {
        return state.nodesData;
    },
    nodes(state) {
        return state.nodes;
    },
}

const actions = {
    async updateNodesData(context) {
        let groups = context.state.nodesData[0];

        /* let grpromises = [];

        for (let i in groups) {
            //console.log(groups, groups[i]);

            grpromises.push(
                new Promise((resolve, reject) => {
                    var group = groups[i];
                    var grName = group.groupobserver;
                    //console.log(grName, group);

                    if (groups[i].observers.length == 0) resolve();

                    //console.log(context.state, context);

                    let promises = [];
                    var lasttime = 0;
                    for (let i in context.rootState.api.servers) {
                        let serv = context.rootState.api.servers[i];
                        //console.log(serv, grName, AorService);

                        promises.push(AorService.getServerObserversByGroup(serv, grName, 0).then((res) => {
                            let arr = JSON.parse(res.data.response);
                            //console.log(arr, { grName: grName, observers: arr.observers, lasttime: arr.lasttime });
                            lasttime = arr.lasttime;
                            context.commit("updateObserversInNodeData", { grName: grName, observers: arr.observers });
                        }).catch(e => {
                            //console.log(e);
                        }));
                    }
                    Promise.all(promises).then(() => {
                        //console.log(lasttime);
                        group.lasttime = lasttime;
                        resolve();
                    });
                })
            );
        } */
/*         return Promise.all(grpromises).then(() => {
            context.commit("updateNodes");
        }); */


        context.commit("updateObserversInNodeData", { grName: "Math", observers: ObserversObj.getObservers() });
        groups.lasttime = (new Date()).getTime();
        context.commit("updateNodes");
    },

    nodeCreated(context, data) {
        context.commit('addNodeToObservers', data);
    },
    addNode(context, node) {
        context.commit('pushNode', node);
        //context.commit("updateNodeDataObject", grName);
    },
    updateNodeDataObject(context, grName) {
        context.commit("updateNodeDataObject", grName);
        context.commit('addNodeToObservers', grName);
    },
    deleteNode(context, id) {
        context.commit('removeNodeFromObservers', id);
        context.commit('removeNode', id);
    },
    clearCanvas(context) {
       // context.commit('deleteAllNodesFromObservers');
        context.commit('deleteAllNodes');
        context.commit('deleteAllElements');
    },
    dataLoadingFromFile(context, groups) {
        context.commit('setGroupsToNodeData', groups);
    },
}

const mutations = {
    pushNode(state, node) {
        state.nodes.push(node);
    },
    removeNodeFromObservers(state, id) {
        var node = state.nodes.find(item => { return item.id === id; });
        if (!node) return;
        var observers = node.properties.observers;

        for (let i in observers) {
            var obs = observers[i];

            if(obs.node == undefined) continue;

            for (let j = 0; j < obs.node.length; j++) {
                const el = obs.node[j];
                if (el.id === id) {
                    obs.node.splice(j, 1);
                    continue;
                }
            }
        }
    },
    removeNode(state, id) {
        for (let i in state.nodes) {
            var node = state.nodes[i];

            if (node.id === id) {
                state.nodes.splice(i, 1);
                return;
            }
        }
    },
    deleteAllNodes() {
        state.nodes = [];
    },
    deleteAllNodesFromObservers() {
        
    },
    updateNodeDataObject(state, grName) {
        //console.log(grName);

        var observers = state.nodes[state.nodes.length - 1].properties.observers;

        var gr = getNodeGroup(state, grName);

        if (!gr) {
            var arrCopy = [];
            for (let i in observers) {
                arrCopy.push(observers[i]);
            }

            state.nodesData.push({
                'groupobserver': grName,
                'observers': arrCopy,
                'lasttime': 0,
            });
            //console.log(state.nodesData);

        } else {
            for (let i in observers) {
                let obs = observers[i];
                //console.log(obs, observers);

                let res = gr.observers.find(item => {
                    return item.name == obs.name;
                });

                if (!res) {
                    gr.observers.push(obs);
                }
            }
        }
        //console.log(state.nodesData);
    },
    addNodeToObservers(state, grName) {
        var observers = state.nodes[state.nodes.length - 1].properties.observers;
        var node = state.nodes[state.nodes.length - 1];

        let gr = getNodeGroup(state, grName);       
        if (!gr) return;

        //console.log(gr, node);

        for (let i in observers) {
            let obs = observers[i];
            //console.log(obs);

            if (obs.node === undefined) {
                Vue.set(obs, 'node', []);
            }
            obs.node.push(node);

            //console.log(obs);
        }
    },
    updateObserversInNodeData(state, data) {
        var grName = data.grName;
        var observers = data.observers;

        for (let i in observers) {
            let obs = observers[i];

            let gr = getNodeGroup(state, grName);
 
            if (!gr) return;

            //console.log('lasttime', gr.lasttime);

            let res = gr.observers.find(item => {
                return item.name == obs.name;
            });

            if (res) {
                res.lt = obs.lt;
                res.value = obs.value;
            }
        }
    },
    setGroupsToNodeData(state, groups) {
        for (let i in groups) {
            state.nodesData.push({
                'groupobserver': groups[i],
                'observers': [],
                'lasttime': 0,
            });
        }
    },
    updateNodes(state) {
        for (let i in state.nodes) {
            state.nodes[i].updateNode();
        }
    },
    moveUpNode(state, index) {
        var node = state.nodes[index];
        state.nodes.splice(index, 1);
        state.nodes.push(node);
    },
}

export default {
    state,
    getters,
    actions,
    mutations
};

function getNodeGroup(state, grName) {
    let gr = state.nodesData.find(item => {
        return item.groupobserver == grName;
    })
    if (!gr) return false;
    return gr;
}