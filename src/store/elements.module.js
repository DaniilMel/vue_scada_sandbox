
const state = {
    elements: [],
    currentElement: undefined,
    selectedElIndex: undefined,
}

const getters = {
    elements(state) {
        return state.elements;
    },
    currentElement(state) {
        return state.currentElement;
    },
    selectedElIndex(state) {
        return state.selectedElIndex;
    },
}

const actions = {
    addElement(context, element) {
        context.commit('setSelectedElementIndex', element.properties.visType.index);
        context.commit('pushElement', element);
        context.commit('setCurrentElement', element);
    },
    editElement(context, element) {
        if (!element) return;
        context.commit('setSelectedElementIndex', element.properties.visType.index);
        context.commit('setCurrentElement', element);
    },
    cancelEditElement(context, element) {
        context.commit('removeSelectedElementIndex');
        context.commit('removeCurrentElement');
    },
    deleteElement(context, element) {
        if (!element) return;
        let id = element.id;
        //console.log(id);
        context.commit('removeCurrentElement', element.properties.visType.index);
        context.commit('deleteElementByID', id);
    },
}

const mutations = {
    deleteAllElements(state) {
        state.elements = [];
        state.currentElement = undefined;
        state.selectedElIndex = undefined;
    },
    setSelectedElementIndex(state, index) {
        state.selectedElIndex = index;
    },
    removeSelectedElementIndex(state) {
        state.selectedElIndex = undefined;
    },
    removeCurrentElement(state) {
        state.currentElement = undefined;
    },
    pushElement(state, element) {
        state.elements.push(element);
    },
    deleteLastElement(state) {
        state.elements.pop();
    },
    deleteElementByID(state, id) {
        for (let i in state.elements) {
            let el = state.elements[i];

            if (el.id == id) {
                state.elements.splice(i, 1);
                return;
            }
        }
    },
    setCurrentElement(state, element) {
        state.currentElement = element;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
};