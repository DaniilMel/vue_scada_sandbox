import Vue from "vue";

const state = {
    settings: {
        nodeInterface: true,
        freezeNodes: false,
        nodeFontOpacity: 0.3,
        showGrid: true,
        gridStep: 10,
        showMap: true
    },
    isLoadCanvasSettings: false,
    canvasSettings: {},
    theme: undefined,
    filename: ''
}

const getters = {
    settings(state) {
        return state.settings;
    },
    theme(state) {
        return state.theme;
    },
    isLoadCanvasSettings(state) {
        return state.isLoadCanvasSettings;
    },
    canvasSettings(state) {
        return state.canvasSettings;
    },
    canvasName(state) {
        return state.filename;
    }
}

const actions = {
    loadCanvasSettings(context, settings) {
        context.commit('setCanvasSettings', settings);
        context.state.isLoadCanvasSettings = true;    
    },
    canvasSettingsUpdated(context) {
        context.state.isLoadCanvasSettings = false;
    }
}

const mutations = {
    setSettings(state, settings) {
        if (!state.settings) state.settings = {};

        for (let f in settings) {
            var val = settings[f];
            Vue.set(state.settings, f, val)
        }
    },
    setCanvasSettings(state, settings) {
        if (!state.canvasSettings) state.canvasSettings = {};

        for (let f in settings) {
            var val = settings[f];
            if(f == 'theme' || f=='gridStep') continue;
            Vue.set(state.canvasSettings, f, val)
        }
    },
    setFileName(state, name) {
        state.filename = name;
    },
    setTheme(state, theme) {
        state.theme = theme;
    },
    setNodeInterface(state, value) {
        state.settings.nodeInterface = value;
    },
    setFreezeNodes(state, value) {
        state.settings.freezeNodes = value;
    },
    setNodeFontOpacity(state, value) {
        state.settings.nodeFontOpacity = value;
    },
    setShowGrid(state, value) {
        state.settings.showGrid = value;
    },
    setGridStep(state, value) {
        state.settings.gridStep = value;
    },
    setShowMap(state, value) {
        state.settings.showMap = value;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
};