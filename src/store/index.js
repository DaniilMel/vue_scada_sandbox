import Vue from "vue";
import Vuex from "vuex";

import menu from "./menu.module";
import nodes from "./nodes.module";
import elements from "./elements.module";
import api from "./api.module";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    menu,
    elements,
    nodes,
    api
  }
});




