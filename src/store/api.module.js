import { AorService } from "../common/api.service";
import { ObserversObj } from "../common/observers/observers_interface";

const state = {
    servers: [],
    groups: [{
        groupobserver: "Math",
        observers: ObserversObj.getObservers()
    }],
    observersByGroups: [
        { group: "Math", observers: ObserversObj.getObservers() }
    ], //{ group: "", observers: [] }
    menuGroupName: "Math",
    savedCanvases: [],
}

const getters = {
    servers(state) {
        return state.servers;
    },
    groups(state) {
        return state.groups;
    },
    observers(state) {
        var res = state.observersByGroups.find(item => { return item.group == state.menuGroupName; });
        return (res ? res.observers : []);
    },
    selectedGroup(state) {
        var res = state.observersByGroups.find(item => { return item.group == state.menuGroupName; });
        return (res ? res.group : []);
    },
    observersByGroups(state) {
        return state.observersByGroups;
    },
    savedCanvases(state) {
        return state.savedCanvases;
    },
}

const actions = {
    getServers(context) {
        return AorService.getServers().then((res) => {
            let arr = JSON.parse(res.data.response).answer.split(' ');
            context.commit("setServers", arr);
        })
    },
    getGroups(context) {
        return AorService.getGroups().then((res) => {
            let arr = JSON.parse(res.data.response).array;
            context.commit("setGroups", arr);
        });
    },
    getSavedCanvases(context) {
        return AorService.getSavedCanvases().then((res) => {
            let arr = JSON.parse(res.data.response).files;
            context.commit("setSavedCanvases", arr);
        });
    },
    async selectGroup(context, grName) {
        if (context.state.servers.length == 0) return;

        if (!grName) {
            context.commit("setDefault");
            return;
        }

        context.commit('setGroup', grName);
        let promises = [];

        for (let i in context.state.servers) {
            let serv = context.state.servers[i];
            promises.push(AorService.getServerObserversByGroup(serv, grName, 0).then((res) => {
                let arr = JSON.parse(res.data.response);
                context.commit("setObservers", { grName: grName, data: arr });
            }));
        }
        await Promise.all(promises);
        return Promise.all(promises);
    },
    async loadObserversByGroups(context, groups) {
        let promises = [];

        for (let i in groups) {
            let grName = groups[i];

            for (let i in context.state.servers) {
                let serv = context.state.servers[i];
                //console.log(serv, grName);

                promises.push(AorService.getServerObserversByGroup(serv, grName, 0).then((res) => {
                    let arr = JSON.parse(res.data.response);
                    context.commit("setObservers", { grName: grName, data: arr });
                }));
            }
        }

        await Promise.all(promises);
        return Promise.all(promises);
    },
    saveCanvas(context, obj) {
        let json = JSON.stringify(obj);
        //console.log(json);

        return AorService.saveJson(json);
    },
    loadCanvas(context, name) {
        return AorService.loadJson(name);
    }
}

const mutations = {
    setServers(state, arr) {
        state.servers = arr;
    },
    setGroups(state, arr) {
        state.groups = arr;
        for (let i in arr) {
            var grName = arr[i].groupobserver;
            state.observersByGroups.push(
                {
                    group: grName,
                    observers: []
                }
            );
        }
    },
    setSavedCanvases(state, arr) {
        state.savedCanvases = arr;
    },
    setGroup(state, grName) {
        state.menuGroupName = grName;
        var res = state.observersByGroups.find(item => { return item.group == state.menuGroupName; });
        if (!res) {
            state.observersByGroups.push(
                {
                    group: grName,
                    observers: []
                }
            );
        }
    },
    setObservers(state, obj) {
        var grName = obj.grName;
        var arr = obj.data.observers;

        let observers = state.observersByGroups.find(item => { return item.group == grName; });

        if (observers) observers = observers.observers;

        for (let i in arr) {
            let obs = arr[i];
            updateArray(observers, 'name', obs, 'name');
        }
    },
    setDefault(state) {
        state.observersByGroups = [];
        state.menuGroupName = '';
    }
}

export default {
    state,
    getters,
    actions,
    mutations
};

function updateArray(target, tfield, subObj, ofield) {
    let res = target.find(item => {
        return item[tfield] == subObj[ofield];
    });

    if (res) {
        for (let f in res) {
            res[f] = subObj[f];
        };
    } else {
        target.push(subObj);
    }
}