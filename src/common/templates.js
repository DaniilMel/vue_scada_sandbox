export const JSONexample = {
    devicesCount: [74],
    devices: [
        {
            name: "НММ",
            address: "VXI0::17::INSTR",
            type: 375,
            kreit: 16,
            LA: 17,
            slot: 2,
            plugName: ""
        },
        {
            name: "МДС32",
            address: "VXI0::17::INSTR",
            type: 9,
            kreit: 16,
            LA: 17,
            slot: 2,
            plugName: ""
        },
        {
            name: "МДС32",
            address: "VXI0::17::INSTR",
            type: 9,
            kreit: 16,
            LA: 17,
            slot: 2,
            plugName: ""
        }
    ]
}

export const observerFields = {
    //:"cc",
    //:"ci",
    //:"cit" ,
    //:"ct",
    FLAG: "fl", //1-красный
    GROUP: "groupobserver",
    TIME: "lt", //время
    NAME: "name", //имя
    VALUE: "value", //значение на вывод
    MAX: "max",
    MIN: "min",
    TYPE:"tv",
    VISUAL:"vv",
}
export const observerFieldsNames = {
    //:"cc",
    //:"ci",
    //:"cit" ,
    //:"ct",
    [observerFields.FLAG]: "Флаг", //1-красный
    [observerFields.GROUP]: "Группа",
    [observerFields.TIME]: "Давность", //время
    [observerFields.NAME]: "Имя", //имя
    [observerFields.VALUE]: "Значение", //значение на вывод
    [observerFields.MAX]: "Макс.",
    [observerFields.MIN]: "Мин.",
    [observerFields.TYPE]:"Тип данных",
    [observerFields.VISUAL]:"Тип визуализации",
}
export const observerFieldsTypes = {
    //:"cc",
    //:"ci",
    //:"cit" ,
    //:"ct",
    [observerFields.FLAG]: "string", //1-красный
    [observerFields.GROUP]: "select",
    [observerFields.TIME]: "date", //время
    [observerFields.NAME]: "string", //имя
    [observerFields.VALUE]: "string", //значение на вывод
    [observerFields.MAX]: "string",
    [observerFields.MIN]: "string",
    [observerFields.TYPE]: "string",
    [observerFields.VISUAL]: "string",
}
export const dataTypes = {
    0: 'BITvalue',
    1: 'INT8value',
    2: 'INT16value',
    3: 'INT32value',
    4: 'INT64value',
    5: 'UINT8value',
    6: 'UINT16value',
    7: 'UINT32value',
    8: 'UINT64value',
    9: 'TIMEvalue',
    10: 'FLOATvalue',
    11: 'STRINGvalue',

    200: 'UNKOWNtype'
}
export const dataVisualisationType = {
    0: '',
    1: 'switch',
    2: 'slider',
    3: 'gauge',
    4: 'datetime',
    5: 'chart'
}

/* {"url":"http://10.120.120.100:8880/v1"} */