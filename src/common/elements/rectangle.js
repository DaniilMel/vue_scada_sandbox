var pointId = 0;

export function Rectangle() {

    this.properties = {
        theme: undefined,
        points: {
            lt: '',
            rt: '',
            lb: '',
            rb: '',
        },
        radius: 0,
        pointRadius: 3,
        colors: {
            border: '7',
            interactive: 'interactive',
        },
        context: '',
        boundingBoxes: {
            lt: '',
            rt: '',
            lb: '',
            rb: ''
        },
    }

    this.setup = function (params) {
        if (params.points) this.properties.points = params.points;

        if (params.theme) this.properties.theme = params.theme;
    }

    this.redraw = function (params, ctx, settings) {

        this.properties.context = ctx;
        let r = this.properties.radius;
        let pr = this.properties.pointRadius;
        let points = this.properties.points;

        let clrBorder = this.properties.theme.getColorByName(  this.properties.colors.border );
        let clrBlue = this.properties.theme.getColorByName(  this.properties.colors.interactive );

        let offsetX = params._x;
        let offsetY = params._y;

        var isCurrent = params.isCurrent;

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = '';

        /* ctx.fillStyle = clrBg;
        const boundingBox = new Path2D();
        boundingBox.rect(zx, zy, w, h);        
        ctx.fill(boundingBox);
        this.properties.boundingBox = boundingBox; */

        /* -------------lines-------------- */
        ctx.strokeStyle = clrBorder;
        ctx.beginPath();
        ctx.moveTo(points.lt.x + offsetX, points.lt.y + offsetY);
        ctx.lineTo(points.rt.x + offsetX, points.rt.y + offsetY);
        ctx.lineTo(points.rb.x + offsetX, points.rb.y + offsetY);
        ctx.lineTo(points.lb.x + offsetX, points.lb.y + offsetY);
        ctx.lineTo(points.lt.x + offsetX, points.lt.y + offsetY);
        ctx.stroke();
        /* --------------/lines-------------- */

        /* --------------points-------------- */
        if (isCurrent) {
            ctx.fillStyle = clrBlue;
            for (let i in points) {
                var point = points[i];
                var x = point.x + offsetX;
                var y = point.y + offsetY;
                ctx.moveTo(x, y);
                ctx.beginPath();
                ctx.arc(x, y, pr, 0, Math.PI * 2, true);
                ctx.fill();
            }
        }
        /* --------------points-------------- */

        for (let f in this.properties.boundingBoxes) {
            var point = points[f];
            var x = point.x + offsetX;
            var y = point.y + offsetY;

            ctx.fillStyle = 'rgba(0,0,0,0)';
            let boundingBox = new Path2D();
            var d = 10;
            boundingBox.rect(x - d, y - d, d * 2, d * 2);
            ctx.fill(boundingBox);
            this.properties.boundingBoxes[f] = boundingBox;
        }
    }

    this.isLTpointSetup = true;
    this.isRBpointSetup = false;

    this.pushPoint = function (point) {
        var pointType = point.id;
        //console.log(point);
        
        this.properties.points[pointType] = point;

        let boundingBox = new Path2D();
        var d = 10;
        boundingBox.rect(point.x - d, point.y - d, d * 2, d * 2);
        this.properties.boundingBoxes[pointType] = boundingBox;
    }
    this.pushPoints = function (points) {
        this.isLTpointSetup = false;
        
        for (let f in points) {
            let point = points[f];
            this.pushPoint(point);
        }
    }
    this.MouseDownHandler = function (e, point) {
        if (this.isLTpointSetup) {
            e.id = 'lt';
            this.pushPoint(e);
            this.isLTpointSetup = false;
            this.isRBpointSetup = true;
            return;
        }
/*         else if (this.isRBpointSetup) {
            e.id = 'rb';
            this.pushPoint(e);
            this.isRBpointSetup = false;
            return;
        } */
    }
    this.MouseMoveHandler = function (e) {
        if (this.isRBpointSetup) {
            let x = e.offsetX;
            let y = e.offsetY;
            let offsetX = e.canvasOffsetX;
            let offsetY = e.canvasOffsetY;
            this.properties.points.rb = { id: 'rb', x: x + offsetX, y: y + offsetY };
            this.properties.points.rt = { id: 'rt', x: x + offsetX, y: this.properties.points.lt.y };
            this.properties.points.lb = { id: 'lb', x: this.properties.points.lt.x, y: y + offsetY };

            //this.setupPoints(this.properties.points.rb);
        }
    }
    this.MouseUpHandler = function (e) {
        this.isRBpointSetup = false;
    }
    this.removePoint = function (point) {
        var id = point.id;

        for (let i in this.properties.points) {
            var point = this.properties.points[i];
            if (point.id == id) {
                this.properties.points.splice(i, 1);
                this.properties.boundingBoxes.splice(i, 1);
            }
        }
    }
    this.setPointPosition = function (id, x, y) {
        var point = this.properties.points[id];
        if (point) {
            point.x = x;
            point.y = y;
        }
        this.setupPoints(point);
    }
    this.getPoint = function (point) {
        let x = point.x;
        let y = point.y;

        for (let f in this.properties.boundingBoxes) {
            var boundingBox = this.properties.boundingBoxes[f];
            var res = this.properties.context.isPointInPath(boundingBox, x, y);
            //console.log(point, res, x, y);

            if (res) {
                if (f == 'rb' && this.isRectCreating) return false;

                return this.properties.points[f];
            }
        }
        return false;
    }

    this.setupPoints = function (point) {
        var id = point.id;
        if (id == 'rt') {
            this.properties.points.rb.x = point.x;
            this.properties.points.lt.y = point.y;
        }
        if (id == 'lt') {
            this.properties.points.rt.y = point.y;
            this.properties.points.lb.x = point.x;
        }
        if (id == 'lb') {
            this.properties.points.lt.x = point.x
            this.properties.points.rb.y = point.y;
        }
        if (id == 'rb') {
            this.properties.points.rt.x = point.x
            this.properties.points.lb.y = point.y;
        }
    }

    this.getZeroPosition = function () {
        /* let x = this.properties.geometry.width / 2;
        let y = this.properties.geometry.height / 2;
        return { x: x, y: y }; */
    }
    this.isHited = function (x, y) {
        for (let f in this.properties.boundingBoxes) {
            var boundingBox = this.properties.boundingBoxes[f];

            var res = this.properties.context.isPointInPath(boundingBox, x, y);

            if (res) {
                return true;
            }
        }
        return false;
    }
}