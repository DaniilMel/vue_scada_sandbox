var pointId = 0;

export function Line() {

    this.block = {
        geometry: {
            height: 70,
            width: 240,
        },
    }

    this.properties = {
        theme: undefined,
        position: {
            x: 0,
            y: 0
        },
        geometry: {
            height: 70,
            width: 240,
        },
        points: [],
        radius: 0,
        pointRadius: 3,
        colors: {
            border: '7',
            interactive: 'interactive',
        },
        context: '',
        boundingBoxes: [],
    }

    this.setup = function (params) {
        if (params.points) this.properties.points = params.points;

        if (params.theme) this.properties.theme = params.theme;
    }

    this.redraw = function (params, ctx, settings) {
        this.properties.context = ctx;
        let r = this.properties.radius;
        let pr = this.properties.pointRadius;
        let points = this.properties.points;

        let clrBorder = this.properties.theme.getColorByName( this.properties.colors.border );
        let clrBlue = this.properties.theme.getColorByName( this.properties.colors.interactive );

        let offsetX = params._x;
        let offsetY = params._y;

        var isCurrent = params.isCurrent;

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = '';

        /* ctx.fillStyle = clrBg;
        const boundingBox = new Path2D();
        boundingBox.rect(zx, zy, w, h);        
        ctx.fill(boundingBox);
        this.properties.boundingBox = boundingBox; */

        /* -------------lines-------------- */
        ctx.strokeStyle = clrBorder;
        ctx.beginPath();
        for (let i in points) {
            var point = points[i];
            var x = point.x + offsetX;
            var y = point.y + offsetY;

            if (i == 0) ctx.moveTo(x, y);


            ctx.lineTo(x, y);
        }
        ctx.stroke();
        /* --------------/lines-------------- */

        /* --------------points-------------- */
        if (isCurrent) {
            ctx.fillStyle = clrBlue;
            for (let i in points) {
                var point = points[i];
                var x = point.x + offsetX;
                var y = point.y + offsetY;
                ctx.moveTo(x, y);
                ctx.beginPath();
                ctx.arc(x, y, pr, 0, Math.PI * 2, true);
                ctx.fill();
            }
        }
        /* --------------points-------------- */

        for (let i in this.properties.boundingBoxes) {
            var point = points[i];
            var x = point.x + offsetX;
            var y = point.y + offsetY;

            //console.log(point.x, x);

            ctx.fillStyle = 'rgba(0,0,0,0)';
            const boundingBox = new Path2D();
            var d = 10;
            boundingBox.rect(x - d, y - d, d * 2, d * 2);
            ctx.fill(boundingBox);
            this.properties.boundingBoxes[i] = boundingBox;
        }
    }
    this.MouseDownHandler = function (e, point) {
        if (!point) this.pushPoint(e);
    }
    this.pushPoint = function (point) {
        point.id = pointId++;
        //console.log('pushPoint', point);
        this.properties.points.push(point);

        const boundingBox = new Path2D();
        var d = 10;
        boundingBox.rect(point.x - d, point.y - d, d * 2, d * 2);
        this.properties.boundingBoxes.push(boundingBox);
    }
    this.pushPoints = function (points) {
        //console.log(points);

        for (let i in points) {
            let point = points[i];

            this.pushPoint(point);
        }
    }
    this.removePoint = function (point) {
        //console.log('remove', point);

        var id = point.id;

        for (let i in this.properties.points) {
            var point = this.properties.points[i];
            if (point.id == id) {
                this.properties.points.splice(i, 1);
                this.properties.boundingBoxes.splice(i, 1);
            }
        }
    }
    this.setPointPosition = function (id, x, y) {
        var point = this.properties.points.find(p => { return p.id == id; });

        //console.log('setPointPosition', point, id, x, y);

        if (point) {
            point.x = x;
            point.y = y;
        }
    }
    this.getPoint = function (point) {
        let x = point.x;
        let y = point.y;

        for (let i in this.properties.boundingBoxes) {
            var boundingBox = this.properties.boundingBoxes[i];

            var res = this.properties.context.isPointInPath(boundingBox, x, y);

            if (res) {
                return this.properties.points[i];
            }
        }
        return false;
    }
    this.isHited = function (x, y) {
        let res = this.properties.boundingBoxes.find(item => { return this.properties.context.isPointInPath(item, x, y); });
        return res != undefined;
    }
}