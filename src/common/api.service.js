import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";

const ApiService = {
  init(r) {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = r;
  },

  get(resource, slug = "") {
    return Vue.axios.get(`${resource}`).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },
};

export default ApiService;

export const AorService = {
  getServers() {
    return Vue.axios.get(`command/КОМАНДЫСЕРВЕРА/GETLISTRESTSERVERS?timeout=1000`);
  },

  getGroups() {
    return Vue.axios.get('command/%20/GET_GROUPOBSERVERS');
  },

  getServerObserversByGroup(server, obsGroup, lt = 0) {
    //console.log(server, obsGroup, lt );

    if (!server) return;
    //Vue.axios.defaults.baseURL = '';
    if (obsGroup) return Vue.axios.get(`http://${server}/v1/command//GET_OBSERVERS?params={"lt":${lt},"group":"${obsGroup}"}`);
  },

  setDataToObserver(name, value) {
    return Vue.axios.post(`command//SET_OBSERVER?params={"value":${value},"name":"${name}"}&timeout=2000`);
  },

  saveJson(json) {
    return Vue.axios.get(`command//SET_JSONTOFILE?params=${json}&timeout=2000`);
  },

  loadJson(name) {
    return Vue.axios.get(`command//GET_JSONFROMFILE?params={"namejson":"${name}"}&timeout=2000`);
  },

  getSavedCanvases() {
    return Vue.axios.get(`command//GET_LISTFILES`);
  }

}

