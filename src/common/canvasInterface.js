export function DrawCanvasInterface(context, params, settings) {
    let ctx = context;
    var offsetX = params.offsetX;
    var offsetY = params.offsetY;
    var theme = params.theme;
    var showGrid = settings.showGrid;

    //var w = ctx.canvas.width;
    //var h = ctx.canvas.height;

    let fullW = ctx.canvas.width + offsetX;
    let fullH = ctx.canvas.height + offsetY;
    let step = params.gridStep;

    var scale = params.scale || 1;
    //var clrBorder = theme.getColorByName('0');
    var clrBg = theme.getColorByName('1');
    var clrAxisDef = theme.getColorByName('2');
    var clrAxisMain = theme.getColorByName('3');

    let rsc = Math.round(scale);
    step *= rsc;

    ctx.fillStyle = '';
    ctx.clearRect(0, 0, fullW, fullH);

    const boundingBox = new Path2D();
    boundingBox.rect(0, 0, fullW, fullH);
    ctx.fillStyle = clrBg;
    ctx.fill(boundingBox);

    //------------------ axis --------------------    
    if (showGrid) {
        let yCount = Math.round(fullH * scale / step);
        let xCount = Math.round(fullW * scale / step);

        // Y
        for (var i = 0; i < yCount; i++) {
            ctx.beginPath();
            ctx.strokeStyle = (i % 10) == 0 ? clrAxisMain : clrAxisDef;
            var y = i * step;
            ctx.moveTo(0, y);
            ctx.lineWidth = 1;
            ctx.lineTo(fullW, y);
            ctx.stroke();
        }
        // X
        for (var i = 0; i < xCount; i++) {
            ctx.beginPath();
            ctx.strokeStyle = (i % 10) == 0 ? clrAxisMain : clrAxisDef;
            var x = i * step;
            ctx.moveTo(x, 0);
            ctx.lineWidth = 1;
            ctx.lineTo(x, fullH);
            ctx.stroke();
        }
    }
    //------------------ /axis --------------------



}

export function CanvasMap() {

    this.properties = {
        boundingRect: '',
        context: '',
        params: ''
    }

    this.redraw = function (context, params, settings, nodes) {
        this.properties.context = context;
        this.properties.params = params;
        let ctx = context;
        var offsetX = params.offsetX;
        var offsetY = params.offsetY;
        var theme = params.theme;
        var maxh = params.maxh;
        var maxw = params.maxw;
        var showMap = settings.showMap; //&& (ctx.canvas.width != maxw || ctx.canvas.height != maxh);

        var w = ctx.canvas.width;
        var h = ctx.canvas.height;
        var k = maxw / maxh;

        var scale = params.scale || 1;
        var clrBorder = theme.getColorByName('0');
        var clrBg = theme.getColorByName('1');
        var clrAxisDef = theme.getColorByName('2');
        var clrAxisMain = theme.getColorByName('3');


        //------------------ map --------------------
        if (showMap) {
            //map

            var mapW = 0;
            var mapH = 0;
            if (w > h) {
                mapW = 200 * scale;
                mapH = mapW / k;
            } else {
                mapH = 120 * scale;
                mapW = mapH / k;
            }

            var mapX = w - mapW - 20 * scale;
            var mapY = h - mapH - 20*scale;

            ctx.fillStyle = theme.getColorWithOpacity(clrAxisMain, 0.6);
            ctx.strokeStyle = clrBorder;

            const boundingRect = new Path2D();
            boundingRect.rect(mapX, mapY, mapW, mapH);
            ctx.stroke(boundingRect);
            ctx.fill(boundingRect);
            this.properties.boundingRect = boundingRect;

            //field
            var fW = w / maxw * mapW;
            var fH = h / maxh * mapH;
            var fX = offsetX / maxw * mapW;
            var fY = offsetY / maxh * mapH;

            ctx.fillStyle = theme.getColorWithOpacity(clrBg, 0.4);

            ctx.beginPath();
            ctx.rect(mapX + fX, mapY + fY, fW, fH);
            ctx.stroke();
            ctx.fill();

            //scale label
            ctx.font = `${16 * scale}px sans-serif`;
            ctx.fillStyle = clrAxisMain;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'top';
            var label = `Масштаб: ${scale.toFixed(1)}`;
            ctx.fillText(label, mapX + mapW / 2, mapY + mapH);

            //nodes
            ctx.fillStyle = theme.getColorWithOpacity(clrBorder, 0.4);
            ctx.strokeStyle = '';

            for (let i in nodes) {
                var node = nodes[i];

                var geom = node.getGeometry();
                var pos = node.getPosition();

                var nW = geom.width / maxw * mapW;
                var nH = geom.height / maxh * mapH;
                var nX = mapX + pos.x / maxw * mapW;
                var nY = mapY + pos.y / maxh * mapH;

                //console.log(pos, nX, nY);
                

                ctx.beginPath();
                ctx.rect(nX, nY, nW, nH);
                ctx.stroke();
                ctx.fill();

            }
        }
        //------------------ /map --------------------
    }

    this.isHited = function (x, y) {
        return this.properties.context.isPointInPath(this.properties.boundingRect, x, y);
    }

    this.getClickAbsolutPos = function (x, y) {
        var w = this.properties.context.canvas.width;
        var h = this.properties.context.canvas.height;
        var scale = this.properties.params.scale || 1;
        var maxh = this.properties.params.maxh;
        var maxw = this.properties.params.maxw;
        var k = maxw / maxh;

        var mapW = 0;
        var mapH = 0;
        if (w > h) {
            mapW = 200 * scale;
            mapH = mapW / k;
        } else {
            mapH = 120 * scale;
            mapW = mapH / k;
        }
        var mapX = w - mapW - 20 * scale;
        var mapY = h - mapH - 20*scale;

        var absX = (x - mapX) / mapW * maxw;
        var absY = (y - mapY) / mapH * maxh;

        return { x: absX, y: absY }
    }

}

export function SelectingRect() {

    this.properties = {
        boundingRect: '',
        context: '',
        params: '',
        nodes: '',
        startPoint: {
            x: undefined,
            y: undefined
        },
        currentPoint: {
            x: undefined,
            y: undefined
        },
        selectedNodes: undefined,
    }

    this.redraw = function (context, params, settings, nodes, elements) {
        this.properties.context = context;
        this.properties.params = params;
        this.properties.nodes = nodes;
        let ctx = context;
        var offsetX = params.offsetX;
        var offsetY = params.offsetY;
        var theme = params.theme;
        var maxh = params.maxh;
        var maxw = params.maxw;

        var sx = this.properties.startPoint.x - offsetX;
        var sy = this.properties.startPoint.y - offsetY;
        var cx = this.properties.currentPoint.x - offsetX;
        var cy = this.properties.currentPoint.y - offsetY;

        var clrBorder = theme.getColorByName('interactive');
        var clrFont = theme.getColorWithOpacity(clrBorder, 0.3);

        ctx.fillStyle = clrFont;
        ctx.strokeStyle = clrBorder;

        ctx.beginPath();
        ctx.rect(sx, sy, cx - sx, cy - sy);
        ctx.stroke();
        ctx.fill();

        this.setSelectedNodes();
    }

    this.setStartPoint = function (x, y) {
        this.properties.startPoint.x = x;
        this.properties.startPoint.y = y;
    }

    this.setCurrentPoint = function (x, y) {
        this.properties.currentPoint.x = x;
        this.properties.currentPoint.y = y;
    }

    this.clearPoints = function () {
        this.properties.startPoint.x = undefined;
        this.properties.startPoint.y = undefined;
        this.properties.currentPoint.x = undefined;
        this.properties.currentPoint.y = undefined;
    }

    this.selectNode = function (node) {
        this.properties.selectedNodes = [];
        node.properties.isSelected = true;
        this.properties.selectedNodes.push(node);
    }

    this.selectNodes = function (nodes) {
        this.unsetSelectedNodes();
        this.properties.selectedNodes = [];

        for (let i = nodes.length - 1; i >= 0; i--) {
            let node = nodes[i];

            node.properties.isSelected = true;
            this.properties.selectedNodes.push(node);
        }
    }

    this.setSelectedNodes = function () {
        var nodes = this.properties.nodes;
        this.properties.selectedNodes = [];

        for (let i in nodes) {
            var node = nodes[i];

            var ltX = node.getPosition().x;
            var ltY = node.getPosition().y;
            var rbX = ltX + node.getGeometry().width;
            var rbY = ltY + node.getGeometry().height;
            var lbX = ltX;
            var lbY = ltY + node.getGeometry().height;
            var rtX = ltX + node.getGeometry().width;
            var rtY = ltY;

            var sx = this.properties.startPoint.x < this.properties.currentPoint.x ? this.properties.startPoint.x : this.properties.currentPoint.x;
            var sy = this.properties.startPoint.y < this.properties.currentPoint.y ? this.properties.startPoint.y : this.properties.currentPoint.y;
            var cx = this.properties.startPoint.x < this.properties.currentPoint.x ? this.properties.currentPoint.x : this.properties.startPoint.x;
            var cy = this.properties.startPoint.y < this.properties.currentPoint.y ? this.properties.currentPoint.y : this.properties.startPoint.y;

            var isSelected = false;

            (() => {
                if ((sx < ltX && ltX < cx) && (sy < ltY && ltY < cy)) {
                    isSelected = true;
                    return;
                }

                if ((sx < rbX && rbX < cx) && (sy < rbY && rbY < cy)) {
                    isSelected = true;
                    return;
                }

                if ((sx < lbX && lbX < cx) && (sy < lbY && lbY < cy)) {
                    isSelected = true;
                    return;
                }

                if ((sx < rtX && rtX < cx) && (sy < rtY && rtY < cy)) {
                    isSelected = true;
                    return;
                }
                
                if ((ltX < sx && sx < rtX) && (ltY <= cy && sy <= lbY) ) {
                    isSelected = true;
                    return;
                }

                if ((ltY < sy && sy < lbY) && (sx <= rtX && cx >= ltX) ) {
                    isSelected = true;
                    return;
                }
            })();

            node.properties.isSelected = isSelected;
            if (isSelected) this.properties.selectedNodes.push(node);

        }
    }

    this.unsetSelectedNodes = function () {
        var nodes = this.properties.selectedNodes;

        for (let i in nodes) {
            var node = nodes[i];
            node.properties.isSelected = false;
        }
        this.properties.selectedNodes = undefined;
    }

    this.isSelecting = function () {
        //console.log(this.properties.selectedNodes);
        var res = this.properties.selectedNodes ? this.properties.selectedNodes.length != 0 : false;
        return res;
    }

}