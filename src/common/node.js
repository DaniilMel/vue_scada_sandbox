import { observerFields } from "./templates";
import { Slider } from "./devices/slider";
/*import { GougeRadial } from "@/src/gaugeRadial";*/
import { Chart } from "./devices/chart";
import { Indicator } from "./devices/indicator";
import { Cooler } from "./devices/cooler";
import { Switch } from "./devices/switch";
import { Value } from "./devices/value";
import { Label } from "./devices/label";
import { TripleSwitch } from "./devices/tripleSwitch";

var scripts = {
    'Chart': Chart,
    /*     'GougeRadial': GougeRadial, */
    'Slider': Slider,
    'Indicator': Indicator,
    'Cooler': Cooler,
    'Switch': Switch,
    'Value': Value,
    'Label': Label,
    'TripleSwitch': TripleSwitch,
}

export const visTypes = [
    {
        label: 'Значение',
        name: 'value',
        index: 0,
        constructor: 'Value'
    },
    {
        label: 'Ключ',
        name: 'switch',
        index: 1,
        constructor: 'Switch'
    },
    {
        label: 'Индикатор',
        name: 'indicator',
        index: 2,
        constructor: 'Indicator'
    },
    {
        label: 'График',
        name: 'chart',
        index: 3,
        constructor: 'Chart'
    },
    {
        label: 'Велтилятор_GIF',
        name: 'cooler',
        index: 4,
        constructor: 'Cooler'
    },
    {
        label: 'Текст',
        name: 'label',
        index: 5,
        constructor: 'Label'
    },
    {
        label: 'Трёхпозиционный ключ',
        name: 'tripleSwitch',
        index: 6,
        constructor: 'TripleSwitch'
    },
    {
        label: 'Круговой индикатор',
        name: 'slider',
        index: 7,
        constructor: 'Slider'
    },
]

var id = 0;

export default function Node() {

    this.id = id++;

    this.properties = {
        group: '',
        observers: [],
        visType: undefined,
        theme: undefined,
        position: {
            x: 0,
            y: 0
        },
        geometry: {
            width: undefined,
            height: undefined
        },
        scale: 0,
        step: 0,
        canvasPattern: '',
        context: '',
        showNodeInterface: true,
        freezeNode: false,
        scalePoints: '',
        deleteButton: '',
        isSelected: false
    }

    this.setupNode = function (params) {
        if (params.group) this.properties.group = params.group;

        if (params.observers) this.properties.observers = params.observers;

        if (params.context) this.properties.context = params.context;

        if (params.theme) this.properties.theme = params.theme;

        if (params.visType !== undefined) {
            //if(typeof params.visType == 'object') this.properties.visType = Object.assign({},params.visType);
            this.setVisTypeObjByTypeIndex(params.visType);
            this.setCanvasPatternByType();
        }

        if (params.geometry) {
            this.properties.geometry = Object.assign({}, params.geometry);
        }

        if (params.position) {
            this.properties.position = Object.assign({}, params.position);
        }

        if (this.properties.canvasPattern.setup !== undefined) {
            this.properties.canvasPattern.setup(
                {
                    context: this.properties.context,
                    theme: this.properties.theme,
                    countValues: this.properties.observers.length,
                    direction: params.direction,
                    label: params.label,
                });
        }
    }

    this.redraw = function (ctx, params, settings) {
        let pattern = this.properties.canvasPattern;
        if (!pattern) return;

        var scale = params.scale;
        var offsetX = params.offsetX;
        var offsetY = params.offsetY;
        this.properties.scale = scale;
        this.properties.step = params.gridStep;

        let pos = this.properties.position;
        let geom = this.properties.geometry;
        let w = geom.width ? geom.width : pattern.properties.geometry.width;
        let h = geom.height ? geom.height : pattern.properties.geometry.height;
        let x = pos.x - offsetX;
        let y = pos.y - offsetY;
        let r = 15;

        pattern.redraw({ _x: x, _y: y, _w: geom.width, _h: geom.height }, ctx, settings);

        this.properties.showNodeInterface = settings.nodeInterface;
        this.properties.freezeNode = settings.freezeNodes;

        /* ----------------draw selectBorder ---------------------- */
        if (this.properties.isSelected) {
            var clrActive = this.properties.theme.getColorByName('interactive');
            ctx.strokeStyle = clrActive;
            ctx.beginPath();
            ctx.rect(x, y, w, h);
            ctx.stroke();
        }
        /* ----------------/draw selectBorder ---------------------- */

        geom = { width: undefined, height: undefined };

        if (!this.properties.showNodeInterface) return;

        /* ----------------draw scale element ---------------------- */
        const scalePoints = new Path2D();
       /*  scalePoints.moveTo(x - w / 2, y - h / 2);
        scalePoints.lineTo(x - w / 2 + r, y - h / 2); scalePoints.lineTo(x - w / 2, y - h / 2 + r); scalePoints.lineTo(x - w / 2, y - h / 2);
        scalePoints.moveTo(x + w / 2, y - h / 2)
        scalePoints.lineTo(x + w / 2 - r, y - h / 2); scalePoints.lineTo(x + w / 2, y - h / 2 + r); scalePoints.lineTo(x + w / 2, y - h / 2);
        scalePoints.moveTo(x - w / 2, y + h / 2)
        scalePoints.lineTo(x - w / 2 + r, y + h / 2); scalePoints.lineTo(x - w / 2, y + h / 2 - r); scalePoints.lineTo(x - w / 2, y + h / 2);
  */    scalePoints.moveTo(x + w, y + h);
        scalePoints.lineTo(x + w - r, y + h); scalePoints.lineTo(x + w, y + h - r); scalePoints.lineTo(x + w, y + h);
        ctx.fillStyle = "rgba(255,255,255,0.8)";
        ctx.strokeStyle = "rgb(0,0,0)";
        ctx.fill(scalePoints);
        ctx.stroke(scalePoints);
        this.properties.scalePoints = scalePoints;
        /* ---------------- /draw scale element ---------------------- */

        /* ----------------draw delete button---------------------- */
        let k = 0.2;
        const deleteButton = new Path2D();
        let btnX = x + w - r;
        let btnY = y;
        deleteButton.rect(btnX, btnY, r, r);
        deleteButton.moveTo(btnX + r * k, btnY + r * k);
        deleteButton.lineTo(btnX + r - r * k, btnY + r - r * k);
        deleteButton.moveTo(btnX + r * k, btnY + r - r * k);
        deleteButton.lineTo(btnX + r - r * k, btnY + r * k);
        ctx.fillStyle = "rgba(180,0,0,0.8)";
        ctx.strokeStyle = "rgb(255,255,255)";
        ctx.fill(deleteButton);
        ctx.stroke(deleteButton);
        this.properties.deleteButton = deleteButton;
        /* ---------------- /draw delete button---------------------- */

    }

    this.updateNode = function () {
        let pattern = this.properties.canvasPattern;
        if (!pattern) return;
        if (this.properties.visType.index == 5) return;

        let values = this.properties.observers.map(item => {
            if (item[observerFields.VISUAL] == 4) {
                let date = new Date(+item[observerFields.VALUE] / 1000);
                let day = date.getUTCDate();
                let month = date.getMonth() + 1;
                let year = date.getFullYear();
                let h = date.getHours();
                let m = date.getMinutes();
                let s = date.getSeconds();
                let ms = date.getMilliseconds();
                return `${day}-${month}-${year}  ${h}:${m}:${s}.${ms}`;//${day}-${month}-${year}  ${h}:${m}:
            }
            return item[observerFields.VALUE];
        });
        let times = this.properties.observers.map(item => { return item[observerFields.TIME] });
        let labels = this.properties.observers.map(item => { return item[observerFields.NAME] });
        let intervals = this.properties.observers.map(item => {
            return {
                max: item.max,
                min: item.min
            };
        });
        pattern.setLabel(labels);
        pattern.pushValue(values, times, intervals);
    }

    this.pushObserver = function (obs) {
        this.properties.observers.push(obs);
    }

    this.getSaveString = function () {
        let copy = {};

        let observers = this.properties.observers.map(item => {
            return item[observerFields.NAME];
        });

        for (let j in this.properties) {
            if (j == "observers") {
                copy[j] = observers;
            } else if (j == 'visType') {
                copy[j] = this.properties[j].index;
            } else if (j == 'geometry' || j == 'position' || j == "group") {
                copy[j] = this.properties[j];
            } else if (j == 'canvasPattern') {
                copy[j] = {};
                copy[j]['label'] = this.properties[j].properties.label;
                copy[j]['direction'] = this.properties[j].properties.direction;
            } else continue;
        }

        return copy;
    }

    this.mouseDownPoint = {};
    this.nodeGeometry = {};
    this.nodePosition = {};
    this.isResizing = false;
    this.isShowPoints = false;

    this.MouseDownHandler = function (e) {
        //console.log("mousedown", e);
        let x = e.offsetX;
        let y = e.offsetY;

        this.mouseDownPoint.x = x;
        this.mouseDownPoint.y = y;

        //arr.unshift(...arr.splice(3,1));

        if (e.button === 0) {
            this.nodeGeometry = Object.assign({}, this.getGeometry());
            this.nodePosition = Object.assign({}, this.getPosition());
        }

        if (!this.properties.showNodeInterface) return;

        if (this.properties.scalePoints) {
            this.isResizing = this.properties.context.isPointInPath(this.properties.scalePoints, x, y);
        }
    }

    this.MouseUpHandler = function (e) {
        this.mouseDownPoint = {};
        this.nodeGeometry = {};
        this.nodePosition = {};
        this.isResizing = false;
    }

    this.MouseMoveHandler = function (e, maxSize) {
        //console.log("move", e);
        let x = e.offsetX;
        let y = e.offsetY;

        if (this.isResizing) {
            //console.log('scale points');


            let dx = x - this.mouseDownPoint.x;
            let dy = y - this.mouseDownPoint.y;

            //console.log(dx, dy);

            let w = this.nodeGeometry.width + dx;
            let h = this.nodeGeometry.height + dy;
            let minw = 50;
            let minh = 50;

            this.properties.geometry.width = w < minw ? minw : w;
            this.properties.geometry.height = h < minh ? minh : h;

        } else {
            if (this.properties.freezeNode) return;
            let dx = x - this.mouseDownPoint.x;
            let dy = y - this.mouseDownPoint.y;

            var ltCornerX = this.nodePosition.x + dx;
            var ltCornerY = this.nodePosition.y + dy;
            var rbCornerX = this.nodePosition.x + dx + this.nodeGeometry.width;
            var rbCornerY = this.nodePosition.y + dy + this.nodeGeometry.height;
            //console.log(ltCornerX, rbCornerX, maxSize.width);

            if (ltCornerX > 0 && rbCornerX < maxSize.width) {
                this.properties.position.x = this.nodePosition.x + dx;
            }
            if (ltCornerY > 0 && rbCornerY < maxSize.height) {
                this.properties.position.y = this.nodePosition.y + dy;
            }

            //console.log(dx, dy, x, y, this.mouseDownPoint, this.nodePosition);

        };
    }
    this.getGeometry = function () {
        return this.properties.geometry.width ? this.properties.geometry : this.properties.canvasPattern.properties.geometry;
    }
    this.getPosition = function () {
        return this.properties.position.x !== undefined ? this.properties.position : this.properties.canvasPattern.properties.position;
    }

    this.setCanvasPatternByType = function () {
        //console.log(this.properties.visType, scripts );

        this.properties.canvasPattern = new scripts[this.properties.visType.constructor];
    }
    this.setVisTypeObjByTypeIndex = function (typeIndex) {
        //console.log(typeIndex, visTypes);

        let visType = visTypes.find(item => { return item.index === typeIndex; });
        if (visType) {
            this.properties.visType = visType;
            //console.log(typeIndex, visTypes, visType);
        }
    }
    this.isHited = function (x, y) {
        if (this.properties.canvasPattern.isHited) {

            return this.properties.canvasPattern.isHited(x, y);
        }

        return false;
    }
    this.isDeleted = function (x, y) {
        if (!this.properties.showNodeInterface) return false;

        return this.properties.context.isPointInPath(this.properties.deleteButton, x, y);
    }
}