import { Line } from "./elements/line";
import { Rectangle } from "./elements/rectangle";

var scripts = {
    'Line': Line,
    'Rectangle': Rectangle,
}

var id = 0;

export default function Element() {

    this.id = id++;

    this.properties = {
        visType: undefined,
        theme: undefined,
        scale: 0,
        step: 0,
        canvasPattern: '',
        context: '',
        freeze: false,
        scalePoints: '',
        deleteButton: '',
    }

    this.setupElement = function (params) {
        if (params.context) this.properties.context = params.context;

        if (params.visType !== undefined) {
            //if(typeof params.visType == 'object') this.properties.visType = Object.assign({},params.visType);
            this.setVisTypeObjByTypeIndex(params.visType);
            this.setCanvasPatternByType();
        }

        if (params.theme) this.properties.theme = params.theme;

        if (this.properties.canvasPattern.setup !== undefined) {
            this.properties.canvasPattern.setup(
                {
                    context: this.properties.context,
                    theme: this.properties.theme,
                });
        }
    }

    this.redraw = function (ctx, params, settings, isCurrent) {
        let pattern = this.properties.canvasPattern;
        if (!pattern) return;

        var scale = params.scale;
        var offsetX = params.offsetX;
        var offsetY = params.offsetY;
        this.properties.scale = scale;
        this.properties.step = params.gridStep;

        let x = - offsetX;
        let y = - offsetY;
        let r = 15;

        pattern.redraw({ _x: x, _y: y, isCurrent: isCurrent }, ctx, settings);

        this.properties.freeze = settings.freezeNodes;
    }

    this.getSaveString = function () {
        let copy = {};

        for (let j in this.properties) {
            if (j == 'visType') {
                copy[j] = this.properties[j].index;
            } else if (j == 'geometry' || j == 'position' || j == "group") {
                copy[j] = this.properties[j];
            } else if (j == 'canvasPattern') {
                copy[j] = {};
                copy[j]['points'] = this.properties[j].properties.points;
            } else continue;
        }

        return copy;
    }

    this.nodeGeometry = {};
    this.nodePosition = {};

    this.mouseDownPoint = {};
    this.selectedPointPos = {};

    this.isPointMove = false;

    this.MouseDownHandler = function (e) {
        //console.log("mousedown", e);
        let x = e.offsetX;
        let y = e.offsetY;
        let offsetX = e.canvasOffsetX;
        let offsetY = e.canvasOffsetY;

        this.mouseDownPoint.x = x;
        this.mouseDownPoint.y = y;

        //arr.unshift(...arr.splice(3,1));

        if (e.button === 0) { //left
            this.selectedPointPos = Object.assign({}, this.getClickedPatternPoint(e));
            //console.log(JSON.stringify(this.selectedPointPos));

            var selected = JSON.stringify(this.selectedPointPos) == '{}' ? false : this.selectedPointPos;

            if (selected) {
                this.isPointMove = true;
            }

            if (this.properties.canvasPattern.MouseDownHandler) {
                this.properties.canvasPattern.MouseDownHandler({ x: x + offsetX, y: y + offsetY }, selected);
            }
        }
        if (e.button === 2) { //right
            var p = Object.assign({}, this.getClickedPatternPoint());

            if (p.id != undefined) {
                this.properties.canvasPattern.removePoint(p)
                return;
            }
        }
    }

    this.MouseUpHandler = function (e) {
        if (this.properties.canvasPattern.MouseUpHandler) {
            this.properties.canvasPattern.MouseUpHandler(e);
        }
        this.mouseDownPoint = {};
        this.selectedPointPos = {};
        this.isPointMove = false;
    }

    this.MouseMoveHandler = function (e) {
        //console.log("move", e);
        let x = e.offsetX;
        let y = e.offsetY;
        let offsetX = e.canvasOffsetX;
        let offsetY = e.canvasOffsetY;

       

        if (this.isPointMove) { 
            if(this.properties.freeze) return;
            
            this.setPointPos(this.selectedPointPos.id, x + offsetX, y + offsetY);
        }

        if (this.properties.canvasPattern.MouseMoveHandler) this.properties.canvasPattern.MouseMoveHandler(e);
    }
    this.getClickedPatternPoint = function (e) {
        return this.properties.canvasPattern.getPoint(this.mouseDownPoint);
    }
    this.setPointPos = function (id, x, y) {
        this.properties.canvasPattern.setPointPosition(id, x, y);
    }

    this.setCanvasPatternByType = function () {
        //console.log(this.properties.visType, scripts);
        this.properties.canvasPattern = new scripts[this.properties.visType.constructor];
    }
    this.setVisTypeObjByTypeIndex = function (typeIndex) {
        let visType = visTypes.find(item => { return item.index == typeIndex });
        if (visType) {
            this.properties.visType = visType;
            //console.log(typeIndex, visTypes, visType);
        }
    }
    this.isHited = function (x, y) {
        if (this.properties.canvasPattern.isHited) {
            return this.properties.canvasPattern.isHited(x, y);
        }

        return false;
    }
    this.isDeleted = function () {

    }
}

export const visTypes = [
    {
        label: 'Линия',
        name: 'line',
        index: 0,
        constructor: 'Line'
    },
    {
        label: 'Прямоугольник',
        name: 'rect',
        index: 1,
        constructor: 'Rectangle'
    },
]