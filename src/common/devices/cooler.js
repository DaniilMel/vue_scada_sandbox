import {
    observerFields,
    observerFieldsTypes,
    observerFieldsNames,
    dataVisualisationType
} from "../templates";
import GIFloader from "../GIFLoader";

export function Cooler() {

    this.block = {
        geometry: {
            height: 150,
            width: 150,
        },
    }

    this.properties = {
        theme: undefined,
        position: {
            x: 0,
            y: 0
        },
        geometry: {
            height: 150,
            width: 150,
        },
        direction: 'y',
        values: undefined,
        labels: null,
        colors: {
            bg: 'rgba(255,255,255,0.5)',
            border: 'rgba(0,0,0,1)',
            true: "rgb(122,204,122)",
            false: "rgb(204,81,81)"
        },
        gifs: [],
        context: '',
        boundingBox: '',
    }

    this.setup = function (params) {
        if (params.position) this.properties.position = params.position;

        if (params.context) this.properties.context = params.context;

        if (params.theme) this.properties.theme = params.theme;

        if (params.direction) this.properties.direction = params.direction;

        if (params.countValues != undefined) {
            this.properties.gifs = new Array(params.countValues);

            for (let i = 0; i < params.countValues; i++) {
                const myGif = GIFloader();
                myGif.src = "./images/fan_1.gif";
                myGif.onload = (event) => {
                    //console.log('load', event, i);
                    this.properties.gifs[i] = event.gif;
                };
            }
        }

    }

    this.redraw = function (params, ctx, settings) {
        this.properties.context = ctx;
        var properties = this.properties;
        var dir = properties.direction;

        this.properties.position.x = params._x; this.properties.position.y = params._y;

        var length = this.properties.values ? this.properties.values.length : 1;
        this.properties.geometry.height = params._h || (dir == 'y' ? this.block.geometry.height * length : this.block.geometry.height);
        this.properties.geometry.width = params._w || (dir == 'x' ? this.block.geometry.width * length : this.block.geometry.width);
        var dh = dir == 'y' ? properties.geometry.height / length : properties.geometry.height;
        var dw = dir == 'x' ? properties.geometry.width / length : properties.geometry.width;
        var h = this.properties.geometry.height;
        var w = this.properties.geometry.width;

        let x = this.properties.position.x;
        let y = this.properties.position.y;

        let labels = this.properties.labels;
        let values = this.properties.values;

        let gifs = this.properties.gifs;

        if (settings) {
            var op = settings.nodeFontOpacity;
            this.properties.colors.bg = this.properties.theme.getBackGroundColor(op);
        }
        let clrBorder = this.properties.colors.border;
        let clrBg = this.properties.colors.bg;

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = clrBorder;
        //ctx.clearRect(cx, cy, w, h);

        const boundingBox = new Path2D();
        boundingBox.rect(x, y, w, h);
        ctx.fillStyle = clrBg;
        ctx.fill(boundingBox)
        this.properties.boundingBox = boundingBox;

        for (let i in values) {
            let value = values[i];
            let label = labels[i];
            var itemZeroPoint = this.getItemZeroPoint(i);
            var itemX = itemZeroPoint.x;
            var itemY = itemZeroPoint.y;

            if (gifs[i]) {
                if(value <= 0) gifs[i].pause();
                else {
                   if(gifs[i].paused) gifs[i].play();
                } 

                ctx.drawImage(gifs[i].image, itemX, itemY, dw, dh);
            }
            //requestAnimationFrame(displayGif);

            if (label !== null) {
                ctx.font = '16px sans-serif';
                ctx.fillStyle = clrBorder;
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.fillText(label, itemX + dw/2, itemY + dh/2);
            }
        }
    }
    this.drawGif = function (ctx, frame) {
        //this.properties.context = ctx;
        //console.log(ctx, frame, this);

        let cx = this.properties.position.x;
        let cy = this.properties.position.y;
        let x = this.properties.position.x + this.properties.geometry.width / 2;
        let y = this.properties.position.y + this.properties.geometry.height / 2;
        let h = this.properties.geometry.height;
        let length = this.properties.value ? this.properties.value.length : 1;
        let dh = h / length;
        let w = this.properties.geometry.width;
        let values = this.properties.value;
        //let k =  this.properties.value.length;
        //console.log(frame.x + cx, frame.y + cy, w, dh, this);
        ctx.globalCompositeOperation = 'source-over';
        for (let i in values) {

            let val = values[i];

            if (val == 0) {

            } else {
                ctx.drawImage(frame.buffer, cx, cy + dh * i, w, dh);
            }


        }


        //ctx.drawImage(frame.buffer, frame.x +0, frame.y + 0, 150, 150);
        //console.log(frame.buffer);

    }

    this.getZeroPosition = function () {
        let x = this.properties.geometry.width / 2;
        let y = this.properties.geometry.height / 2;
        return { x: x, y: y };
    }

    this.pushValue = function (valuesArr) {
        this.properties.values = valuesArr;
    }
    this.setLabel = function (value) {
        this.properties.labels = value;
    }

    this.isHited = function (x, y) {
        //console.log(x, y, this.properties.context.isPointInPath(this.properties.boundingBox, x, y));
        return this.properties.context.isPointInPath(this.properties.boundingBox, x, y);
    }
    

    this.getItemZeroPoint = function (i) {
        var pos = this.properties.position;
        var geom = this.properties.geometry;
        var valuesNumber = this.properties.values.length;
        var dir = this.properties.direction;

        if (valuesNumber == undefined) {
            return { 'x': pos.x, 'y': pos.y };
        }

        if (!dir || dir == 'y') {
            var dh = geom.height / valuesNumber;
            var y = pos.y + dh * i;
            var x = pos.x;
            return { 'x': x, 'y': y };
        } else {
            var dw = geom.width / valuesNumber;
            var x = pos.x + dw * i;
            var y = pos.y;
            return { 'x': x, 'y': y };
        }
    }
}
