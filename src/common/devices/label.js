export function Label() {

    this.block = {
        geometry: {
            height: 70,
            width: 140,
        },
    }

    this.properties = {
        theme: undefined,
        position: {
            x: 0,
            y: 0
        },
        geometry: {
            height: 70,
            width: 140,
        },
        value: undefined,
        label: null,
        colors: {
            bg: '',
            font: '8',
        },
        context: '',
        boundingBox: '',
    }

    this.setup = function (params) {
        if (params.position) this.properties.position = params.position;

        if(params.label) this.properties.label = params.label;

        if (params.theme) this.properties.theme = params.theme;
    }

    this.redraw = function (params, ctx, settings) {
        this.properties.position.x = params._x; this.properties.position.y = params._y;
        this.properties.geometry.width = params._w || this.properties.geometry.width;
        this.properties.geometry.height = params._h || this.properties.geometry.height;

        if (settings) {
            var op = settings.nodeFontOpacity;
            this.properties.colors.bg =this.properties.theme.getBackGroundColor(op);
        }

        this.properties.context = ctx;
        let zx = this.properties.position.x;
        let zy = this.properties.position.y;
        let x = this.properties.position.x + this.properties.geometry.width / 2;
        let y = this.properties.position.y + this.properties.geometry.height / 2;
        let h = this.properties.geometry.height;
        let w = this.properties.geometry.width;

        let label = this.properties.label;
        //console.log(x,y,zx,zy);

        let clrFont = this.properties.theme.getColorByName( this.properties.colors.font );
        let clrBg = this.properties.colors.bg;

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = '';

        ctx.fillStyle = clrBg;
        const boundingBox = new Path2D();
        boundingBox.rect(zx, zy, w, h);
        ctx.fill(boundingBox);
        this.properties.boundingBox = boundingBox;
        
        if (label !== null) {
            ctx.font = '16px sans-serif';
            ctx.fillStyle = clrFont;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.fillText(label, x , y);
        }
    }

    this.pushValue = function (value) { }
    this.setLabel = function (value) {
        this.properties.label = value;
    }

    this.getZeroPosition = function () {
        let x = this.properties.geometry.width / 2;
        let y = this.properties.geometry.height / 2;
        return { x: x, y: y };
    }
    this.isHited = function (x, y) {
        return this.properties.context.isPointInPath(this.properties.boundingBox, x, y);
    }
}