import { isNull } from "util";

export function Chart() {

    this.properties = {
        'theme': undefined,
        'colors': {
            bg: 'rgba(60,60,60,1)',
            axes: 'rgba(240,240,240,1)',
            horiz: 'rgba(130,130,130,1)',
            label: 'rgba(240,240,240,1)',
        },
        'fnStringToValue': function (string) { return parseInt(string); },
        'fnValueToString': function (value) { return value.toString(); },
        'label': null,
        'needle': false,
        'readonly': false,
        'textScale': 1.0,
        'trackWidth': 0.4,

        'minVal': 0,
        'maxVal': 0,
        'data': [[]],
        'dataLength': 100,
        'showValue': null,

        'axesY': [],
        'axesX': [],
        'axisH': 20,
        'deltaVisual': 0,
        'valOffset': 0,

        'height': 0,
        'width': 0,
        'x': 0,
        'y': 0,
        'offsetX': 38,
        'offsetY': 25,
        'img': null,
        geometry: {
            height: 200,
            width: 400,
        },
        position: {
            x: 0,
            y: 0
        },
        context: '',
        boundingBox: ''
    };

    this.setup = function (params) {
        if (params.theme) this.properties.theme = params.theme;
    }

    this.redraw = function (params, _ctx, settings) {
        //console.log(_x, _y, _w, _h, );
        this.properties.position.x = params._x; this.properties.position.y = params._y;
        let _w = params._w || this.properties.geometry.width; let _h = params._h || this.properties.geometry.height;
        let _x = params._x; let _y = params._y;
        //let length = this.properties.data[0] ? this.properties.data[0].length : 1;
        this.updateProps();

        var offsetX = this.properties.offsetX;
        var offsetY = this.properties.offsetY;
        this.properties.x = _x + offsetX;
        this.properties.y = _y + offsetY;
        var properties = this.properties;

        var height = _h - offsetY * 2;
        var width = _w - offsetX;
        var x = properties.x;
        var y = properties.y;

        this.properties.height = height; this.properties.width = width;

        var ctx = _ctx; this.properties.context = ctx;
        var needle = properties.needle;

        /* var value = properties.val;
        var valueToString = properties.fnValueToString;
        var valueStr = valueToString(value); */
        var valMin = properties.minVal;//
        var valMax = properties.maxVal;//

        var colorBG = properties.colors.bg;
        var colorAxes = properties.colors.axes;
        var colorLabel = properties.colors.label;
        var colorHoriz = properties.colors.horiz;

        let labels = this.properties.label;
        var textScale = properties.textScale;
        var smaller = width < height ? width : height;
        var centerX = 0.5 * _w + _x; //
        var centerY = 0.5 * _h + _y; //
        var labelY = centerY;
        var fontSize = (0.2 * smaller) * textScale;
        var fontSizeString = fontSize.toString();

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = '';

        //Draw font         
        const boundingBox = new Path2D();
        boundingBox.rect(_x, _y, _w, _h);
        ctx.fillStyle = colorBG;
        ctx.fill(boundingBox);
        this.properties.boundingBox = boundingBox;

        //Draw Y axes
        for (let i in this.properties.axesY) {
            ctx.beginPath();
            let axY = this.properties.axesY[i];
            let dy = y + height - axY.pos * height;
            dy = (dy % 10 < 5 ? Math.round(dy) + 0.5 : Math.round(dy) - 0.5);
            ctx.moveTo(x, dy);
            //console.log(dy);


            ctx.font = "12px serif";
            ctx.fillStyle = colorLabel;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.fillText(axY.value, x - offsetX / 2, dy);

            ctx.lineWidth = 1;
            ctx.strokeStyle = colorHoriz;
            ctx.lineTo(x + width, dy);
            ctx.stroke();
        }

        //console.log(this.properties.axesY);

        for (let index in this.properties.data) {
            //Draw points
            ctx.beginPath();
            for (let i in this.properties.data[index]) {
                let point = this.properties.data[index][i];

                let dx = x + (width / (this.properties.dataLength - 1)) * (parseInt(i) + this.properties.dataLength - this.properties.data[index].length); //отступ по X
                let py = point.y == undefined ? undefined : (point.y - this.properties.valOffset) / properties.deltaVisual;
                let dy = py == undefined ? undefined : y + height - (py * height);
                //console.log(x, width);
                //console.log(dy, dx, i , this.properties.dataLength , this.properties.data.length);

                if (i) {
                    if (dy == undefined) {
                        continue;
                    } else if (i > 1 && this.properties.data[index][i - 1].y == undefined) {

                    } else {
                        ctx.strokeStyle = this.getColor(index);//colorLines
                        ctx.lineTo(dx, dy);
                        ctx.stroke();
                    }
                }
                ctx.beginPath();
                ctx.arc(dx, dy, 1, 0, Math.PI * 2, true);
                ctx.moveTo(dx, dy);
                ctx.fill();

                if (this.showPointValue()) {
                    ctx.font = "12px serif";
                    ctx.fillStyle = colorLabel;
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    if (typeof point.y == 'string') {
                        ctx.fillText(point.y, dx, dy - 10);
                    } else if (typeof point.y == 'number') {
                        ctx.fillText(point.y.toFixed(2).toString(), dx, dy - 10);
                    }
                }

                if (index == this.properties.data.length - 1) {

                    //Draw X axes
                    if (this.showAxesXValue() && (i < this.properties.data[index].length - 1)) {
                        let yX = y + height + 8;
                        ctx.fillStyle = colorLabel;
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        ctx.fillText(point.x, dx, yX);
                    }
                }
            }

            //Draw the label
            if (labels !== null) {
                let maxLabelWidth = 0;
                let fontSize = 14;
                let margin = 5;
                ctx.font = `${fontSize}px sans-serif`;
                ctx.textAlign = 'left';
                ctx.textBaseline = 'top';

                for (let i in labels) {
                    let label = labels[i];

                    let lw = ctx.measureText(label).width;
                    if (lw > maxLabelWidth) maxLabelWidth = lw;
                }

                let labelWidth = (fontSize + maxLabelWidth + margin) * labels.length;
                let labelX = centerX - labelWidth / 2;

                for (let i in labels) {
                    let label = labels[i];
                    let marginI = i * (maxLabelWidth + fontSize + margin);

                    ctx.beginPath();
                    ctx.fillStyle = this.getColor(i);
                    ctx.rect(labelX + marginI, _y + 5, fontSize, fontSize);
                    ctx.fill();

                    ctx.fillStyle = colorLabel;
                    ctx.fillText(label, fontSize + labelX + marginI, _y + 5);
                    ctx.closePath();

                }

            }
        }

        //Draw axes
        ctx.beginPath();
        ctx.strokeStyle = colorAxes;
        ctx.moveTo(x, y);
        ctx.lineTo(x, y + height);
        ctx.lineTo(x + width, y + height);
        ctx.stroke();
    }

    this.updateProps = function () {
        for (let index in this.properties.data) {
            for (let i in this.properties.data[index]) { //определение макс и мин значений массива
                let point = this.properties.data[index][i];
                this.properties.maxVal = point.y > this.properties.maxVal ? point.y : this.properties.maxVal;
                this.properties.minVal = point.y < this.properties.minVal ? point.y : this.properties.minVal;
            }
        }
        //console.log(this.properties.data, this.properties.maxVal, this.properties.minVal);
        let delta = (this.properties.maxVal - this.properties.minVal) * 1.1; // разница между макс и мин значениями
        let axesNumber = Math.ceil(this.properties.height / this.properties.axisH) + 1; //кол-во горизонтальных линий на графике
        let k = Math.pow(10, (roundUp(Math.log10(axesNumber)) - (delta != 0 ? roundUp(Math.log10(delta)) : 0) + 2)); //коэффициент масштабирования
        this.properties.deltaVisual = (delta * k + (axesNumber - 1) - delta * k % (axesNumber - 1)) / k; //отображаемый диапазон от мин до макс
        let valPerAxes = this.properties.deltaVisual / (axesNumber - 1); //цена деления
        let offset = roundUp(this.properties.minVal / valPerAxes) * valPerAxes;  //смещение осей относительно 0 axesNumber * (Math.trunc(this.properties.minVal / axesNumber) + ((this.properties.minVal % axesNumber != 0) ? Math.sign(this.properties.minVal)*1 : 0))
        this.properties.valOffset = offset;

        //console.log(delta, axesNumber,k, this.properties.deltaVisual,valPerAxes, offset);

        this.properties.axesY = [];
        for (let i = 0; i < axesNumber; i++) {
            let axesY = this.properties.axesY;
            //console.log(i * valPerAxes + offset);

            axesY.push({
                value: (i * valPerAxes + offset).toFixed(2),
                pos: i / (axesNumber - 1)
            });
        }
    }

    this.pushValue = function (values, times) {
        for (let i in values) {
            let value = values[i];

            let data = this.properties.data[i];
            // console.log(i, value, this.properties.data);


            if (data == undefined) this.properties.data.push([]);

            data = this.properties.data[i];

            if (data.length >= this.properties.dataLength) data.shift();

            let t = times ? this.getTime(times[i]) : this.getTime();
            data.push({ x: t, y: value });
        }
    }

    this.setLabel = function (val) {
        this.properties.label = val;
    }
    this.showPointValue = function () {
        if (isNull(this.properties.showValue)) return (this.properties.width / this.properties.dataLength) > 30;

        return this.properties.showValue;
    }
    this.showAxesXValue = function () {
        if (isNull(this.properties.showValue)) return (this.properties.width / this.properties.dataLength) > 30;

        return this.properties.showValue;
    }
    this.getTime = function (t) {
        let date = t ? (new Date(+t / 1000)) : (new Date());

        //var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

        /* let day = date.getUTCDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
    
        let h = date.getHours();
        let m = date.getMinutes(); */
        let s = date.getSeconds();
        let ms = date.getMilliseconds();

        return `${s}.${ms}`;//${day}-${month}-${year}  ${h}:${m}:
    }
    this.getZeroPosition = function () {
        let x = this.properties.geometry.width / 2;
        let y = this.properties.geometry.height / 2;
        return { 'x': x, 'y': y };
    }
    this.getColor = function (i) {
        return `hsl(${Math.floor(360 * i / this.properties.data.length)},100%,50%)`
    }
    this.isHited = function (x, y) {
        //console.log(x, y, this.properties.context.isPointInPath(this.properties.boundingBox, x, y));

        return this.properties.context.isPointInPath(this.properties.boundingBox, x, y);
    }
}

function roundUp(val) {
    return val < 0 ? Math.floor(val) : Math.ceil(val);
}