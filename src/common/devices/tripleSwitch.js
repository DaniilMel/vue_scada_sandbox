import {
    observerFields,
    observerFieldsTypes,
    observerFieldsNames,
    dataVisualisationType
} from "../templates";

export function TripleSwitch() {

    this.block = {
        geometry: {
            height: 150,
            width: 150,
        }
    }

    this.properties = {
        theme: undefined,
        position: {
            x: 0,
            y: 0
        },
        geometry: {
            height: 150,
            width: 150,
        },
        direction: 'y',
        values: undefined,
        labels: null,
        colors: {
            bg: '',
            border: '8',
            font: 'rgba(60,60,60,1)'
        },
        images: [
            {
                src: './images/triple_switch/0.png',
                condition: (val) => { return val <= 1; },
                imgObj: ''
            },
            {
                src: './images/triple_switch/1.png',
                condition: (val) => { return val > 1 && val <= 4; },
                imgObj: ''
            },
            {
                src: './images/triple_switch/2.png',
                condition: (val) => { return val > 4 && val <= 7; },
                imgObj: ''
            },
            {
                src: './images/triple_switch/3.png',
                condition: (val) => { return val > 7; },
                imgObj: ''
            },
        ],
        context: '',
        boundingBox: '',
    }

    this.setup = function (params) {
        if (params.position) this.properties.observers = params.position;

        if (params.theme) this.properties.theme = params.theme;

        if (params.direction) this.properties.direction = params.direction;

        for (let i = 0; i < this.properties.images.length; i++) {
            let img = this.properties.images[i];
            img.imgObj = new Image(10, 10);
            img.imgObj.onload = (event) => {
                //console.log('load', event, i);
                //this.properties.images[i].imgObj = event;
            };
            img.imgObj.src = img.src;
        }
    }

    this.redraw = function (params, ctx, settings) {
        this.properties.context = ctx;
        var properties = this.properties;
        var dir = properties.direction;

        this.properties.position.x = params._x; this.properties.position.y = params._y;

        var length = this.properties.values ? this.properties.values.length : 1;
        this.properties.geometry.height = params._h || (dir == 'y' ? this.block.geometry.height * length : this.block.geometry.height);
        this.properties.geometry.width = params._w || (dir == 'x' ? this.block.geometry.width * length : this.block.geometry.width);
        var dh = dir == 'y' ? properties.geometry.height / length : properties.geometry.height;
        var dw = dir == 'x' ? properties.geometry.width / length : properties.geometry.width;
        var h = this.properties.geometry.height;
        var w = this.properties.geometry.width;

        let x = this.properties.position.x;
        let y = this.properties.position.y;

        let labels = this.properties.labels;
        let values = this.properties.values;

        if (settings) {
            var op = settings.nodeFontOpacity;
            this.properties.colors.bg = this.properties.theme.getBackGroundColor(op);
        }
        let clrBorder = this.properties.theme.getColorByName(this.properties.colors.border);
        let clrFont = this.properties.colors.font;
        let clrBg = this.properties.colors.bg;

        let images = this.properties.images;

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = clrBorder;
        //ctx.clearRect(cx, cy, w, h);

        const boundingBox = new Path2D();
        boundingBox.rect(x, y, w, h);
        ctx.fillStyle = clrBg;
        ctx.fill(boundingBox);
        this.properties.boundingBox = boundingBox;

        for (let i in values) {
            let value = values[i];
            let label = labels[i];
            var itemZeroPoint = this.getItemZeroPoint(i);
            var itemX = itemZeroPoint.x;
            var itemY = itemZeroPoint.y;

            for (let j in images) {
                const img = images[j];
                if (!img.imgObj) continue;

                if (img.condition(value)) {
                    ctx.drawImage(img.imgObj, itemX, itemY, dw, dh);
                    continue;
                }
            }

            if (label !== null) {
                ctx.font = '16px sans-serif';
                ctx.fillStyle = clrFont;
                ctx.textAlign = 'center';
                ctx.textBaseline = 'top';
                ctx.fillText(label, itemX + dw / 2, itemY);
            }
        }
    }

    this.pushValue = function (valuesArr) {
        this.properties.values = valuesArr;
    }
    this.setLabel = function (labelsArr) {
        this.properties.labels = labelsArr;
    }

    this.getZeroPosition = function () {
        let x = 0;//this.properties.geometry.width / 2;
        let y = 0;//this.properties.geometry.height / 2;
        return { x: x, y: y };
    }
    this.isHited = function (x, y) {
        return this.properties.context.isPointInPath(this.properties.boundingBox, x, y);
    }


    this.getItemZeroPoint = function (i) {
        var pos = this.properties.position;
        var geom = this.properties.geometry;
        var valuesNumber = this.properties.values.length;
        var dir = this.properties.direction;

        if (valuesNumber == undefined) {
            return { 'x': pos.x, 'y': pos.y };
        }

        if (!dir || dir == 'y') {
            var dh = geom.height / valuesNumber;
            var y = pos.y + dh * i;
            var x = pos.x;
            return { 'x': x, 'y': y };
        } else {
            var dw = geom.width / valuesNumber;
            var x = pos.x + dw * i;
            var y = pos.y;
            return { 'x': x, 'y': y };
        }
    }
}