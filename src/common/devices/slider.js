export function Slider() {

    this.block = {
        geometry: {
            height: 150,
            width: 150,
        },
    }

    this.properties = {
        'angleEnd': 2.0 * Math.PI,
        'angleOffset': -0.5 * Math.PI,
        'angleStart': 0,
        'colorBG': 'rgba(0,0,0,1)',//'#181818',
        'colorFG': 'rgba(220,140,60,1)',
        'colorLabel': 'rgba(255,255,255,1)',
        'fnStringToValue': function (string) { return parseInt(string); },
        'fnValueToString': function (value) { return value.toString(); },
        'textScale': 1.0,
        'trackWidth': 0.4,
        theme: undefined,
        position: {
            x: 0,
            y: 0
        },
        geometry: {
            height: 150,
            width: 150,
        },
        direction: 'y',
        values: undefined,
        intervals: [],
        labels: null,
        colors: {
            bg: 'rgba(255,255,255,0.5)',
            border: '8',
        },
        context: '',
        boundingBox: '',
    };

    this.setup = function (params) {
        if (params.position) this.properties.observers = params.position;

        if (params.theme) this.properties.theme = params.theme;

        if (params.direction) this.properties.direction = params.direction;
    }

    this.redraw = function (params, _ctx, settings) {
        var properties = this.properties;
        var dir = properties.direction;

        this.properties.context = _ctx;
        var ctx = _ctx;

        let _x = params._x; let _y = params._y;
        this.properties.position.x = params._x; this.properties.position.y = params._y;

        var length = this.properties.values ? this.properties.values.length : 1;
        this.properties.geometry.height = params._h || (dir == 'y' ? this.block.geometry.height * length : this.block.geometry.height);
        this.properties.geometry.width = params._w || (dir == 'x' ? this.block.geometry.width * length : this.block.geometry.width);
        var dh = dir == 'y' ? properties.geometry.height / length : properties.geometry.height;
        var dw = dir == 'x' ? properties.geometry.width / length : properties.geometry.width;
        var h = this.properties.geometry.height;
        var w = this.properties.geometry.width;

        var colorTrack = properties.colorBG;
        var colorFilling = properties.colorFG;
        var colorLabel = properties.colorLabel;
        if (settings) {
            var op = settings.nodeFontOpacity;
            this.properties.colors.bg = this.properties.theme.getBackGroundColor(op);
        }
        let clrBorder = this.properties.theme.getColorByName(this.properties.colors.border);
        let clrBg = this.properties.colors.bg;

        var angleStart = properties.angleStart;
        var angleOffset = properties.angleOffset;
        var angleEnd = properties.angleEnd;
        var actualStart = angleStart + angleOffset;
        var actualEnd = angleEnd + angleOffset;

        var textScale = properties.textScale;
        var trackWidth = properties.trackWidth;

        var labels = properties.labels;
        var values = properties.values;
        var intervals = properties.intervals;

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = clrBorder;

        const boundingBox = new Path2D();
        boundingBox.rect(_x, _y, w, h);
        ctx.fillStyle = clrBg;
        ctx.fill(boundingBox);
        this.properties.boundingBox = boundingBox;

        for (let i in values) {
            var value = values[i];
            var label = labels[i];
            var itemZeroPoint = this.getItemZeroPoint(i);
            var x = itemZeroPoint.x;
            var y = itemZeroPoint.y;

            var valueStr = value != undefined ? properties.fnValueToString(value.toFixed(1)) : "";
            var valMin = intervals[i].min;
            var valMax = intervals[i].max;
            var relValue = value != undefined ? (value - valMin) / (valMax - valMin) : 0;
            var relAngle = relValue * (angleEnd - angleStart);
            var angleVal = actualStart + relAngle;

            var smaller = dw < dh ? dw : dh;
            var centerX = 0.5 * dw + x;
            var centerY = 0.5 * dh + y;
            var radius = 0.4 * smaller;

            var labelY = centerY + radius;
            var lineWidth = Math.round(trackWidth * radius);
            var labelSize = Math.round(0.8 * lineWidth);
            var labelSizeString = labelSize.toString();
            var fontSize = (0.2 * smaller) * textScale;
            var fontSizeString = fontSize.toString();
            /*
             * Draw the track.
             */
            ctx.beginPath();
            ctx.arc(centerX, centerY, radius, actualStart, actualEnd);
            ctx.lineCap = 'butt';
            ctx.lineWidth = lineWidth;
            ctx.strokeStyle = colorTrack;
            ctx.stroke();
            /*
             * Draw the filling.
             */
            /*
             * Check if we're in needle mode.
             */
            ctx.beginPath();
            ctx.arc(centerX, centerY, radius, actualStart, angleVal);
            ctx.lineCap = 'butt';
            ctx.lineWidth = lineWidth;
            ctx.strokeStyle = colorFilling;
            ctx.stroke();
            /*
             * Draw the number.
             */
            ctx.font = fontSizeString + 'px sans-serif';
            ctx.fillStyle = colorFilling;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.fillText(valueStr, centerX, centerY);
            /*
             * Draw the label
             */
            if (label !== null) {
                ctx.font = labelSizeString + 'px sans-serif';
                ctx.fillStyle = colorLabel;
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.fillText(label, centerX, labelY);
            }

            ctx.lineWidth = 1;
        }
    }

    this.mouseEventToValue = function (e) {
        var width = this.properties.width;
        var height = this.properties.height;
        var centerX = 0.5 * width + this.properties.x;
        var centerY = 0.5 * height + this.properties.y;
        var x = e.x;
        var y = e.y;
        var relX = x - centerX;
        var relY = y - centerY;
        var angleStart = properties.angleStart;
        var angleEnd = properties.angleEnd;
        var angleDiff = angleEnd - angleStart;
        var angle = Math.atan2(relX, -relY) - angleStart;
        var twoPi = 2.0 * Math.PI;
        /*
         * Make negative angles posistive.
         */
        if (angle < 0) {

            if (angleDiff >= twoPi)
                angle += twoPi;
            else
                angle = 0;
        }
        var valMin = properties.valMin;
        var valMax = properties.valMax;
        var value = ((angle / angleDiff) * (valMax - valMin)) + valMin;
        /*
         * Clamp values into valid interval.
         */
        if (value < valMin)
            value = valMin;
        else if (value > valMax)
            value = valMax;

        return value;
    };

    this.pushValue = function (arr, times, intervals) {
        this.properties.values = arr;
        this.properties.intervals = intervals;

        for(let i in intervals) {
            var item = intervals[i];

            if(item.min == 0 && item.max == 0) {
                if(arr[i] > item.max) item.max = arr[i];
                if(arr[i] < item.min) item.min = arr[i];
            }
        }
    }

    this.setLabel = function (arr) {
        this.properties.labels = arr;
    }

    this.getZeroPosition = function () {
        let x = this.properties.geometry.width / 2;
        let y = this.properties.geometry.height / 2;
        return { x: x, y: y };
    }
    this.isHited = function (x, y) {
        return this.properties.context.isPointInPath(this.properties.boundingBox, x, y);
    }

    this.getItemZeroPoint = function (i) {
        var pos = this.properties.position;
        var geom = this.properties.geometry;
        var valuesNumber = this.properties.values.length;
        var dir = this.properties.direction;

        if (valuesNumber == undefined) {
            return { 'x': pos.x, 'y': pos.y };
        }

        if (!dir || dir == 'y') {
            var dh = geom.height / valuesNumber;
            var y = pos.y + dh * i;
            var x = pos.x;
            return { 'x': x, 'y': y };
        } else {
            var dw = geom.width / valuesNumber;
            var x = pos.x + dw * i;
            var y = pos.y;
            return { 'x': x, 'y': y };
        }
    }
}