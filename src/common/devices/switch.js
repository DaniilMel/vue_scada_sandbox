import {
    observerFields,
    observerFieldsTypes,
    observerFieldsNames,
    dataVisualisationType
} from "../templates";

export function Switch() {
    this.block = {
        geometry: {
            height: 100,
            width: 100,
        },
    }

    this.properties = {
        theme: undefined,
        position: {
            x: 0,
            y: 0
        },
        geometry: {
            height: 100,
            width: 100,
        },
        direction: 'y',
        values: undefined,
        labels: null,
        colors: {
            bg: 'rgba(255,255,255,0.5)',
            border: '8',
        },
        context: '',
        boundingBox: '',
    }

    this.setup = function (params) {
        if (params.position) this.properties.observers = params.position;

        if (params.theme) this.properties.theme = params.theme;

        if (params.direction) this.properties.direction = params.direction;
    }

    this.redraw = function (params, ctx, settings) {
        this.properties.context = ctx;
        var properties = this.properties;
        var dir = properties.direction;

        this.properties.position.x = params._x; this.properties.position.y = params._y;
        var length = this.properties.values ? this.properties.values.length : 1;
        this.properties.geometry.height = params._h || (dir == 'y' ? this.block.geometry.height * length : this.block.geometry.height);
        this.properties.geometry.width = params._w || (dir == 'x' ? this.block.geometry.width * length : this.block.geometry.width);
        var dh = dir == 'y' ? properties.geometry.height / length : properties.geometry.height;
        var dw = dir == 'x' ? properties.geometry.width / length : properties.geometry.width;
        var h = this.properties.geometry.height;
        var w = this.properties.geometry.width;
        let x = this.properties.position.x;
        let y = this.properties.position.y;
        let r = 2;

        let labels = this.properties.labels;
        let values = this.properties.values;

        if(settings) {
            var op = settings.nodeFontOpacity;
            this.properties.colors.bg = this.properties.theme.getBackGroundColor(op);
        }
        let clrBorder = this.properties.theme.getColorByName(  this.properties.colors.border );
        let clrBg = this.properties.colors.bg;

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = clrBorder;

        const boundingBox = new Path2D();
        boundingBox.rect(x, y, w, h);
        ctx.fillStyle = clrBg;
        ctx.fill(boundingBox);
        this.properties.boundingBox = boundingBox;

        for (let i in values) {
            let value = values[i];
            let label = labels[i];
            var itemZeroPoint = this.getItemZeroPoint(i);
            var itemX = itemZeroPoint.x;
            var itemY = itemZeroPoint.y;
            let lineStartY = itemY + dh/2;

            ctx.lineWidth = 1;
            ctx.beginPath();
            //ctx.strokeRect(x, y, w, dh*(k+1));
            ctx.moveTo(itemX, lineStartY);
            ctx.lineTo(itemX + dw / 4, lineStartY);
            ctx.stroke();

            ctx.lineWidth = 3;
            ctx.beginPath();
            ctx.arc(itemX + dw / 4, lineStartY, r, 0, Math.PI * 2, true);
            if (value) ctx.lineTo(itemX + 3 * dw / 4, lineStartY);
            else ctx.lineTo(itemX + 3 * dw / 4, lineStartY - dh / 4);
            ctx.stroke();

            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(itemX + 3 * dw / 4, lineStartY);
            ctx.lineTo(itemX + dw, lineStartY);
            ctx.stroke();

            if (label !== null) {
                ctx.font = '16px sans-serif';
                ctx.fillStyle = clrBorder;
                ctx.textAlign = 'center';
                ctx.textBaseline = 'top';
                ctx.fillText(label, itemX + dw/2, lineStartY - 0.8 * dh / 2);
            }
        }


    }

    this.pushValue = function (arr) {
        this.properties.values = arr;
    }
    this.setLabel = function (arr) {
        this.properties.labels = arr;
    }

    this.getZeroPosition = function () {
        let x = this.properties.geometry.width / 2;
        let y = this.properties.geometry.height / 2;
        return { x: x, y: y };
    }
    this.isHited = function (x, y) {
        return this.properties.context.isPointInPath(this.properties.boundingBox, x, y);
    }

    this.getItemZeroPoint = function (i) {
        var pos = this.properties.position;
        var geom = this.properties.geometry;
        var valuesNumber = this.properties.values.length;
        var dir = this.properties.direction;

        if (valuesNumber == undefined) {
            return { 'x': pos.x, 'y': pos.y };
        }

        if (!dir || dir == 'y') {
            var dh = geom.height / valuesNumber;
            var y = pos.y + dh * i;
            var x = pos.x;
            return { 'x': x, 'y': y };
        } else {
            var dw = geom.width / valuesNumber;
            var x = pos.x + dw * i;
            var y = pos.y;
            return { 'x': x, 'y': y };
        }
    }
}