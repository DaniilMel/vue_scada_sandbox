import {
    observerFields,
    observerFieldsTypes,
    observerFieldsNames,
    dataVisualisationType
} from "../templates";

export function Value() {

    this.block = {
        geometry: {
            height: 70,
            width: 240,
        },
    }

    this.properties = {
        theme: undefined,
        position: {
            x: 0,
            y: 0
        },
        geometry: {
            height: 70,
            width: 240,
        },
        direction: 'y',
        values: undefined,
        labels: null,
        colors: {
            bg: 'rgba(255,255,255,0.5)',
            border: '8',
        },
        context: '',
        boundingBox: '',
    }

    this.setup = function (params) {
        if (params.position) this.properties.observers = params.position;

        if (params.theme) this.properties.theme = params.theme;

        if (params.direction) this.properties.direction = params.direction;
    }

    this.redraw = function (params, ctx, settings) {
        this.properties.context = ctx;
        var properties = this.properties;
        var dir = properties.direction;

        this.properties.position.x = params._x; this.properties.position.y = params._y;
        var length = this.properties.values ? this.properties.values.length : 1;
        this.properties.geometry.height = params._h || (dir == 'y' ? this.block.geometry.height * length : this.block.geometry.height);
        this.properties.geometry.width = params._w || (dir == 'x' ? this.block.geometry.width * length : this.block.geometry.width);
        var dh = dir == 'y' ? properties.geometry.height / length : properties.geometry.height;
        var dw = dir == 'x' ? properties.geometry.width / length : properties.geometry.width;
        var h = this.properties.geometry.height;
        var w = this.properties.geometry.width;
        let x = this.properties.position.x;
        let y = this.properties.position.y;


        let labels = this.properties.labels;
        let values = this.properties.values;

        if (settings) {
            var op = settings.nodeFontOpacity;
            this.properties.colors.bg = this.properties.theme.getBackGroundColor(op);
        }
        let clrBorder = this.properties.theme.getColorByName(this.properties.colors.border);
        let clrBg = this.properties.colors.bg;

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = '';

        ctx.fillStyle = clrBg;
        const boundingBox = new Path2D();
        boundingBox.rect(x, y, w, h);
        ctx.fill(boundingBox);
        this.properties.boundingBox = boundingBox;

        for (let i in values) {
            let value = `${(typeof values[i] == 'number') ? values[i].toFixed(2) : values[i]}`;
            let label = `${labels[i]}:  `;
            var itemZeroPoint = this.getItemZeroPoint(i);
            var itemX = itemZeroPoint.x;
            var itemY = itemZeroPoint.y;
 
            ctx.font = '16px sans-serif';
            ctx.fillStyle = clrBorder;
            ctx.textAlign = 'left';
            ctx.textBaseline = 'middle';
            var lblwidth = ctx.measureText(label).width;
            var valwidth = ctx.measureText(value).width;
            ctx.fillText(label, itemX + 10, itemY + dh / 2);
            ctx.fillText(value, itemX + dw - valwidth - 10, itemY + dh / 2);
        }
    }

    this.pushValue = function (arr) {
        this.properties.values = arr;
    }
    this.setLabel = function (arr) {
        this.properties.labels = arr;
    }
    this.getZeroPosition = function () {
        let x = this.properties.geometry.width / 2;
        let y = this.properties.geometry.height / 2;
        return { x: x, y: y };
    }
    this.isHited = function (x, y) {
        return this.properties.context.isPointInPath(this.properties.boundingBox, x, y);
    }


    this.getItemZeroPoint = function (i) {
        var pos = this.properties.position;
        var geom = this.properties.geometry;
        var valuesNumber = this.properties.values.length;
        var dir = this.properties.direction;

        if (valuesNumber == undefined) {
            return { 'x': pos.x, 'y': pos.y };
        }

        if (!dir || dir == 'y') {
            var dh = geom.height / valuesNumber;
            var y = pos.y + dh * i;
            var x = pos.x;
            return { 'x': x, 'y': y };
        } else {
            var dw = geom.width / valuesNumber;
            var x = pos.x + dw * i;
            var y = pos.y;
            return { 'x': x, 'y': y };
        }
    }
}