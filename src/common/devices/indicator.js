import {
    observerFields,
    observerFieldsTypes,
    observerFieldsNames,
    dataVisualisationType
} from "../templates";

export function Indicator() {

    this.block = {
        geometry: {
            height: 50,
            width: 50,
        },
    }

    this.properties = {
        position: {
            x: 0,
            y: 0
        },
        geometry: {
            height: 50,
            width: 50,
        },
        direction: 'y',
        values: [],
        times: [],
        labels: null,
        theme: undefined,
        colors: {
            bg: 'rgba(255,255,255,0.5)',
            border: '8',
            true: "true",
            false: "false",
            waite: '4'
        },
        currentColors: [],
        context: '',
        boundingBox: '',
    }

    this.setup = function (params) {
        if (params.position) this.properties.position = params.position;

        if (params.theme) this.properties.theme = params.theme;

        if (params.direction) this.properties.direction = params.direction;
    }

    this.redraw = function (params, ctx, settings) {
        this.properties.context = ctx;
        var properties = this.properties;
        var dir = properties.direction;

        this.properties.position.x = params._x; this.properties.position.y = params._y;
        var length = this.properties.values ? this.properties.values.length : 1;
        this.properties.geometry.height = params._h || (dir == 'y' ? this.block.geometry.height * length : this.block.geometry.height);
        this.properties.geometry.width = params._w || (dir == 'x' ? this.block.geometry.width * length : this.block.geometry.width);
        var dh = dir == 'y' ? properties.geometry.height / length : properties.geometry.height;
        var dw = dir == 'x' ? properties.geometry.width / length : properties.geometry.width;
        var h = this.properties.geometry.height;
        var w = this.properties.geometry.width;
        let x = this.properties.position.x;
        let y = this.properties.position.y;

        let labels = this.properties.labels;
        let values = this.properties.values;

        if (settings) {
            var op = settings.nodeFontOpacity;
            this.properties.colors.bg = this.properties.theme.getBackGroundColor(op);
        }
        let clrTrue = this.properties.theme.getColorByName(this.properties.colors.true);
        let clrFalse = this.properties.theme.getColorByName(this.properties.colors.false);
        let clrBorder = this.properties.theme.getColorByName(this.properties.colors.border);
        let clrBg = this.properties.colors.bg;
        let clrs = this.properties.currentColors;

        //clear canvas props
        ctx.fillStyle = '';
        ctx.strokeStyle = clrBorder;
        //ctx.clearRect(cx, cy, w, h);

        const boundingBox = new Path2D();
        boundingBox.rect(x, y, w, h);
        ctx.fillStyle = clrBg;
        ctx.fill(boundingBox)
        this.properties.boundingBox = boundingBox;

        for (let i in values) {
            let value = values[i];
            let label = labels[i];
            var itemZeroPoint = this.getItemZeroPoint(i);
            var itemX = itemZeroPoint.x;
            var itemY = itemZeroPoint.y;

            ctx.beginPath();
            //ctx.strokeRect(cx, cy, w, dh*(k+1));

            this.roundRect(ctx, itemX + dw * 0.1, itemY + dh * 0.1, dw * 0.8, dh * 0.8, 10, false);
            //ctx.arc(x, cy + dh*k + dh/2, r, 0, Math.PI * 2, true);
            ctx.fillStyle = clrs[i] ? clrs[i] : clrFalse;
            ctx.fill();

            if (label !== null) {
                ctx.font = '16px sans-serif';
                ctx.fillStyle = clrBorder;
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.fillText(label, itemX + dw / 2, itemY + dh / 2);
            }
        }
    }

    this.roundRect = function (ctx, x, y, width, height, radius, fill, stroke) {
        if (typeof stroke == 'undefined') {
            stroke = true;
        }
        if (typeof radius === 'undefined') {
            radius = 5;
        }
        if (typeof radius === 'number') {
            radius = { tl: radius, tr: radius, br: radius, bl: radius };
        } else {
            var defaultRadius = { tl: 0, tr: 0, br: 0, bl: 0 };
            for (var side in defaultRadius) {
                radius[side] = radius[side] || defaultRadius[side];
            }
        }
        ctx.beginPath();
        ctx.moveTo(x + radius.tl, y);
        ctx.lineTo(x + width - radius.tr, y);
        ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
        ctx.lineTo(x + width, y + height - radius.br);
        ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
        ctx.lineTo(x + radius.bl, y + height);
        ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
        ctx.lineTo(x, y + radius.tl);
        ctx.quadraticCurveTo(x, y, x + radius.tl, y);
        ctx.closePath();
        if (fill) {
            ctx.fill();
        }
        if (stroke) {
            ctx.stroke();
        }
    }

    this.getZeroPosition = function () {
        let x = this.properties.geometry.width / 2;
        let y = this.properties.geometry.height / 2;
        return { x: x, y: y };
    }

    this.pushValue = function (values, newtimes) {
        let p = this.properties;
        if (p.values.length != values.length) p.values = new Array(values.length);
        if (p.currentColors.length != values.length) p.currentColors = new Array(values.length);
        if (p.times.length != values.length) p.times = new Array(values.length);

        for (let i in values) {
            var value = values[i];
            let bool = value > 0 ? true : false;

            var lastVal = p.values[i];

            p.currentColors[i] = p.theme.getColorByName(bool ? p.colors.true : p.colors.false);

            if (lastVal != bool) {
                p.values[i] = bool;
                p.times[i] = newtimes[i];
                continue;
            }

            /* if (p.times[i] != newtimes[i]) {
                p.currentColors[i] = p.theme.getColorByName(!bool ? p.colors.true : p.colors.false);
                setTimeout(() => {
                    p.currentColors[i] = p.theme.getColorByName(bool ? p.colors.true : p.colors.false);
                }, 400);
            } */
            p.times[i] = newtimes[i];
        }
    }
    this.setLabel = function (arr) {
        this.properties.labels = arr;
    }

    this.isHited = function (x, y) {
        //console.log(x, y, this.properties.context.isPointInPath(this.properties.boundingBox, x, y));

        return this.properties.context.isPointInPath(this.properties.boundingBox, x, y);
    }



    this.getItemZeroPoint = function (i) {
        var pos = this.properties.position;
        var geom = this.properties.geometry;
        var valuesNumber = this.properties.values.length;
        var dir = this.properties.direction;

        if (valuesNumber == undefined) {
            return { 'x': pos.x, 'y': pos.y };
        }

        if (!dir || dir == 'y') {
            var dh = geom.height / valuesNumber;
            var y = pos.y + dh * i;
            var x = pos.x;
            return { 'x': x, 'y': y };
        } else {
            var dw = geom.width / valuesNumber;
            var x = pos.x + dw * i;
            var y = pos.y;
            return { 'x': x, 'y': y };
        }
    }
}