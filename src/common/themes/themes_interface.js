import light from "./light";
import dark from "./dark";

export function Themes(themeName) {

    this.list = [
        light,
        dark
    ]

    this.currentTheme = this.list[0];

    this.name = function() {
        return this.currentTheme.name;
    }
    this.label = function() {
        return this.currentTheme.label;
    }

    this.setThemeByName = function (name) {
        var res = this.list.find(item => { return item.name == name; });
        if(!res) return;
        this.currentTheme = res;
    }

    this.getThemeByName = function (name) {
        let res = this.list.find(item => {
            return item.name == name;
        });
        return res != undefined ? res : false;
    }

    this.getColors = function () {
        return this.currentTheme.colors;
    }

    this.getColorByName = function (clrname) {
        var colors = this.getColors();
        let clr = colors.find(item => { return item.name === clrname; });
        return clr != undefined ? clr.color : false;
    }

    this.getBackGroundColor = function (opacity) {
        var clrMain = this.getColorByName('0');
        if (clrMain.indexOf('rgba') == -1) return clrMain;

        var newClr = clrMain.slice(0, -2);
        return `${newClr}${ opacity != undefined ? opacity : 0.5 })`;
    }

    this.getColorWithOpacity = function ( color, opacity ) {
        if (color.indexOf('rgba') == -1) return color;

        var newClr = color.slice(0, -2);
        return `${newClr}${ opacity != undefined ? opacity : 0.5 })`;
    }

    if (themeName != undefined) {
        var res = this.getThemeByName(themeName);
        if(res) this.currentTheme = res;
    }

}