import axios from "axios";
var url = '';

async function getUrl() {

    let hName = window.location.hostname;

    let response;
    let host;

    const config = () => import("../../public/data");
    await config().then( r => { 
        response = r; 
    });

    if (hName == 'localhost' || hName == '127.0.0.1') {
        host = response.host;
    } else {
        host = hName;
    }

    url = response.protocol + host + response.port + response.postfix;
    
    return url;
};

export function initURL() {
    return getUrl();
}
export function returnUrl() {
    return url;
};