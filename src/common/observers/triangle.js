export function Triangle(starttime) {

    this.properties = {
        name: "Triangle",
        starttime: starttime,
        max: 10,
        min: 0,
        lasttime: starttime,
        value: 0,
    }

    this.getValue = function(currenttime, k) {
        var prop = this.properties;
        var t = (currenttime - prop.starttime)*k;
        prop.value +=  0.5;
        prop.value = prop.value > prop.max ? prop.min : prop.value;

        return prop.value;
    }
}