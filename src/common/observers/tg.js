export function Tg(starttime) {

    this.properties = {
        name: "Tg()",
        starttime: starttime,
        max: 10,
        min: -10,
        lasttime: starttime,
    }

    this.getValue = function(currenttime, k) {
        var prop = this.properties;
        var amplitude = prop.max - prop.min;

        var t = (currenttime - prop.starttime)*k;
        var value = Math.tan(t);
        
        value = value > prop.max ? undefined : value;
        value = value < prop.min ? undefined : value;

        return value;
    }
}