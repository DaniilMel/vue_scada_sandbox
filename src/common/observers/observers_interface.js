import { Sin } from "./sin";
import { Cos } from "./cos";
import { Rect } from "./rect";
import { Tg } from "./tg";
import { Triangle } from "./triangle";
var classes = [
    Sin,
    Cos,
    Rect,
    Tg,
    Triangle
]

function Observers() {

    this.properties = {
        starttime: '',
        observersObjs: [],
        observers: [],
        timeK: 0.001
    }


    var prop = this.properties;
    prop.starttime = (new Date()).getTime();
    for (let i in classes) {
        var constructor = classes[i];
        prop.observersObjs.push(new constructor(prop.starttime));
    }
    for (let i in prop.observersObjs) {
        var obs = prop.observersObjs[i];

        let obj = {
            name: obs.properties.name,
            lt: obs.properties.lasttime,
            min: obs.properties.min,
            max: obs.properties.max,
            value: obs.getValue(prop.starttime),
        }

        prop.observers.push(obj);
    }

    this.getObservers = function () {
        var curTime = (new Date()).getTime();
        var prop = this.properties;
       
        for (let i in prop.observersObjs) {
            var obsObj = prop.observersObjs[i];
            var obs = prop.observers[i];
            obsObj.properties.lasttime = curTime;

            obs.value = obsObj.getValue(curTime, prop.timeK);
            obs.lt = curTime;
            obs.min = obsObj.properties.min;
            obs.max = obsObj.properties.max;
        }
        return prop.observers;
    }
}

export const ObserversObj = new Observers();