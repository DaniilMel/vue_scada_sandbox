export function Rect(starttime) {

    this.properties = {
        name: "Rect",
        starttime: starttime,
        max: 10,
        min: 0,
        lasttime: starttime,
    }

    this.getValue = function(currenttime, k) {
        var prop = this.properties;
        var t = (currenttime - prop.starttime)*k;
        var value = Math.cos(t*2);
        value = value > 0 ? prop.max : prop.min;

        return value;
    }
}