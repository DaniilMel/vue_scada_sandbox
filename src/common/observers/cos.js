export function Cos(starttime) {

    this.properties = {
        name: "Cos()",
        starttime: starttime,
        max: 10,
        min: -10,
        lasttime: starttime,
    }

    this.getValue = function(currenttime, k) {
        var prop = this.properties;
        var amplitude = prop.max - prop.min;

        var t = (currenttime - prop.starttime)*k;
        var value = amplitude/2 * Math.cos(t);

        return value;
    }
}